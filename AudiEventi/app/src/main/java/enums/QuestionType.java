package enums;

public enum QuestionType {
    WEBFORM_LIKERT,
    RADIOS,
    WEBFORM_RADIOS_OTHER,
    CHECKBOXES,
    WEBFORM_CHECKBOXES_OTHER,
    TEXTAREA
}
