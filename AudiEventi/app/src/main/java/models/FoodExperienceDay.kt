package models

data class FoodExperienceDay(
    val day: String,
    val experience: Experience
)
