package models

data class Meta(
    val alt: String,
    val title: String,
    val width: String,
    val height: String
)