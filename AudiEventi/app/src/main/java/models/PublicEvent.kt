package models

import kotlin.collections.HashMap

data class PublicEvent(
    val id: String,
    val title: String,
    val description: HashMap<String, String>, // format, value
    val eventDate: String, // 2021-02-27
    val linkMyAudi: HashMap<String, Any>,
    val priority: Int,
    val status: String,
    val backgroundImage: HashMap<String, Any>
)
