package models

data class Contact(
    val id: Int,
    val title: HashMap<String, String>, // contains the title of the contacts - it is not shown by the App
    val freeText: HashMap<String, String>,
    val contactImage: HashMap<String, Any>
)
