package models

data class Link(
    val uri: String,
    val title: String,
    val options: Any
)
