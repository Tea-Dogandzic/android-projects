package models

data class FoodExperience(
    val id: String,
    val title: String,
    val header: String,
    val subtitleFood: HashMap<String, String>,
    val foodImage: HashMap<String, Any>,
    val programmeExperience: ArrayList<FoodExperienceDay>
)
