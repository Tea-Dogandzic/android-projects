package models

// You get information about the survey.
// List of questions with possible answers.
// Status: Open, Closed, Empty
data class Survey(
    val questions: ArrayList<Question>,
    val status: String
)
