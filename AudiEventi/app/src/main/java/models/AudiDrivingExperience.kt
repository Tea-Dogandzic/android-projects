package models

data class AudiDrivingExperience(
    val data: Any, // both events
    val id: String,
    val freeText: String,
    val subtitleAde: String,
    val titleAde: String,
    val carouselAde: ArrayList<BackgroundImage>,
    val imageAde: BackgroundImage
)
