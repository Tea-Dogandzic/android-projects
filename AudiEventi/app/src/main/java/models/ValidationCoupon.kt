package models

data class ValidationCoupon(
    val action: String,
    val coupon: String,
    val eventId: String,
    val status: String,
    val valid: Char,
    val result: String,
    val resultCode: String,
    val resultMessage: String
)
