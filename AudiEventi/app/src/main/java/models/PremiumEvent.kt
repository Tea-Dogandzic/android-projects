package models

data class PremiumEvent(
    val id: String,
    val title: String,
    val description: HashMap<String, String>,
    val headerPremium: String,
    val linkMyAudiPremium: ArrayList<Link>,
    val programmeNote: HashMap<String, String>,
    val detailedPlan: ArrayList<HashMap<String, Any>>,
    val subtitle: String,
    val image: HashMap<String, Any>
)
