package models

import enums.QuestionType

data class Question(
    val webformId: Int, // surveyId
    val title: String,
    val questionId: Int,
    val required: Boolean,
    val requiredError: String,
    val multipleWebform: Boolean,
    val type: QuestionType
) {
}