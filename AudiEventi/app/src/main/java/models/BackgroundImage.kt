package models

data class BackgroundImage(
    val id: String,
    val href: String,
    val meta: Meta
)

