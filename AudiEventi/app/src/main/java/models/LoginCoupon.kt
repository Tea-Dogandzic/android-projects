package models

data class LoginCoupon(
    val jwtToken: String
)
