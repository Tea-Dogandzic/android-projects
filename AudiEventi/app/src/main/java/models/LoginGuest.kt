package models

data class LoginGuest(
    val clientId: String,
    val clientSecret: String
)

// ovo se salje, a dobija se jwt token
