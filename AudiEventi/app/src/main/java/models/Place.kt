package models

data class Place(
    val id: String,
    val title: String,
    val description: String,
    val placeSubtitle: String,
    val placeTitle: String,
    val carouselPlace: ArrayList<BackgroundImage>,
    val imagePlace: BackgroundImage
)
