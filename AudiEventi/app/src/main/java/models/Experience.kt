package models

data class Experience(
    val startTime: String,
    val type: String, // enum?
    val activity: String,
    val place: String,
    val description: String,
    val food: String, // html format
    val allergens: String // html format
)
