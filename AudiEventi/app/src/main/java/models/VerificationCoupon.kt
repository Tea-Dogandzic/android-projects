package models

data class VerificationCoupon(
    val coupon: String,
    val eventId: String,
    val status: String, // zameniti sa enum “INACTIVE”,”ACTIVE”,”USED”
    val valid: String, // enum "T", "F"
    val eventStatus: String, // “I“: Inattivo, “A”: Attivo,  ”S”: Scaduto
    val result: String, // “OK”, “KO”
    val resultCode: String,
            //    “” : stringa vuota , nessun problema, result vale “OK”
            //“err000” : Si e’ verificato un errore inatteso all’interno del CMS
            //“err001” : L’evento associato al coupon ha lo stato Non Attivo
            //“err002” : L’evento associato al coupon ha lo stato Scaduto
    val resultMessage: String
)