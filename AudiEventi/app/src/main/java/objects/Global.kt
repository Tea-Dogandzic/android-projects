package objects

import models.*
import utils.Utils

object global {
    val utils: Utils = Utils()

    var langs: HashMap<String, String> = hashMapOf()

    val baseUrl = "http://mobileapp-coll.engds.it/AudiEventi"
    val langsUrl = "/langs/it_IT.json"
    val configUrl = "/config/config.json"

    val testPublicEventData: ArrayList<PublicEvent> = arrayListOf()
    val testPremiumEventData: ArrayList<PremiumEvent> = arrayListOf()
    val testFoodExperienceData: ArrayList<FoodExperience> = arrayListOf()
    val testAudiDrivingExperienceData: ArrayList<AudiDrivingExperience> = arrayListOf()
    val testPlacesAndTerritoryData: ArrayList<Place> = arrayListOf()

    var showDialog = false

    var premiumUser = false
}