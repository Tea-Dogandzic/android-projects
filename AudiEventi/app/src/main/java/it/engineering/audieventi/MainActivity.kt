package it.engineering.audieventi

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import objects.global
import org.json.JSONObject
import utils.TestDataGenerator
import java.net.HttpURLConnection
import java.net.URL

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        downloadJSON().execute(global.utils.concatenateUrl(global.baseUrl, global.langsUrl))
    }

    inner class downloadJSON: AsyncTask<String, String, String>() {
        override fun doInBackground(vararg params: String?): String {
            var json: String
            val connection = URL(params[0]).openConnection() as HttpURLConnection

            try {
                connection.connect()
                json = connection.inputStream.use {
                    it.reader().use {
                            reader -> reader.readText()
                    }
                }
            } finally {
                connection.disconnect()
            }

            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            if (result != null) {
                global.utils.parseJSON(JSONObject(result))
                val testDataGenerator: TestDataGenerator = TestDataGenerator()
                testDataGenerator.readPublicEventsData(this@MainActivity)
                testDataGenerator.readPremiumEventsData(this@MainActivity)
                testDataGenerator.readFoodExperienceData(this@MainActivity)
                testDataGenerator.readAudiDrivingExperienceData(this@MainActivity)
                testDataGenerator.readPlacesAndTerritoryData(this@MainActivity)
                startActivity(Intent(this@MainActivity, HomeGuestActivity::class.java))
            }
        }
    }
}