package it.engineering.audieventi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import objects.global
import utils.CustomDialog

class HomeGuestActivity : AppCompatActivity() {
    lateinit var toggle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_guest)


        /* HAMBURGER MENU */

        val navDrawerLayout = findViewById<DrawerLayout>(R.id.drawerMenuLayout)
        val navigationView = findViewById<NavigationView>(R.id.navigationView)

        toggle = ActionBarDrawerToggle(this, navDrawerLayout, R.string.open, R.string.close)
        navDrawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        findViewById<ImageView>(R.id.btnHamburgerMenu).setOnClickListener {
            navDrawerLayout.openDrawer(navigationView)
        }

        navigationView.setNavigationItemSelectedListener {
            when(it.itemId){
                R.id.home -> {
                    navDrawerLayout.closeDrawer(navigationView)
                }
                R.id.calendar -> {
                    startActivity(Intent(this, PublicEventsActivity::class.java))
                }
                R.id.coupon -> {
                    navDrawerLayout.closeDrawer(navigationView)
                    CustomDialog().show(supportFragmentManager, "MyCustomFragment")
                }
            }

            true
        }


        /* POPULATE VIEW */

        val eventiAudiLayout = findViewById<LinearLayout>(R.id.eventi_audi_layout)
        val eventiAudiImage = findViewById<ImageView>(R.id.eventi_audi_image)
        val eventiAudiTitle = findViewById<TextView>(R.id.eventi_audi_title)
        val eventiAudiSubtitle = findViewById<TextView>(R.id.eventi_audi_subtitle)

        val ilTuoEventoLayout = findViewById<LinearLayout>(R.id.il_tuo_evento_layout)
        val ilTuoEventoImage = findViewById<ImageView>(R.id.il_tuo_evento_image)
        val ilTuoEventoTitle = findViewById<TextView>(R.id.il_tuo_evento_title)
        val ilTuoEventoSubtitle = findViewById<TextView>(R.id.il_tuo_evento_subtitle)

        val gammaAudiLayout = findViewById<LinearLayout>(R.id.gamma_audi_layout)
        val gammaAudiImage = findViewById<ImageView>(R.id.gamma_audi_image)
        val gammaAudiTitle = findViewById<TextView>(R.id.gamma_audi_title)
        val gammaAudiSubtitle = findViewById<TextView>(R.id.gamma_audi_subtitle)

        eventiAudiTitle.text = global.langs.get("HOME_GUEST_EVENT_LIST")
        eventiAudiSubtitle.text = global.langs.get("HOME_GUEST_EVENT_LIST_BUTTON")
        eventiAudiImage.setImageResource(R.drawable.eventi_home_guest)

        ilTuoEventoTitle.text = global.langs.get("HOME_GUEST_COUPON_LOGIN")
        ilTuoEventoSubtitle.text = "Inserisci coupon"
        ilTuoEventoImage.setImageResource(R.drawable.evento_coupon_home_guest)

        gammaAudiTitle.text = global.langs.get("HOME_GUEST_AUDI_RANGE")
        gammaAudiSubtitle.text = global.langs.get("HOME_GUEST_AUDI_RANGE_BUTTON")
        gammaAudiImage.setImageResource(R.drawable.gamma_audi_home)


        /* SET ACTIONS */

        eventiAudiLayout.setOnClickListener {
            startActivity(Intent(this, PublicEventsActivity::class.java))
        }

        ilTuoEventoLayout.setOnClickListener {
            CustomDialog().show(supportFragmentManager, "MyCustomFragment")
        }

        gammaAudiLayout.setOnClickListener {
            // TO DO
        }

        findViewById<LinearLayout>(R.id.startSettingsActivity).setOnClickListener {
            startActivity(Intent(this, SettingsActivity::class.java))
        }

        if (global.showDialog) {
            CustomDialog().show(supportFragmentManager, "MyCustomFragment")
            global.showDialog = false
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)){
            return true
        }

        return super.onOptionsItemSelected(item)
    }
}