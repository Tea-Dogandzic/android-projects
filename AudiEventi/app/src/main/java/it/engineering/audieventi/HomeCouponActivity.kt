package it.engineering.audieventi

import android.content.Intent
import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import objects.global
import utils.CustomDialog

class HomeCouponActivity : AppCompatActivity() {
    lateinit var toggle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_coupon)

        populateData()

        /* HAMBURGER MENU */

        val navDrawerLayout = findViewById<DrawerLayout>(R.id.drawerMenuLayout)
        val navigationView = findViewById<NavigationView>(R.id.navigationView)

        toggle = ActionBarDrawerToggle(this, navDrawerLayout, R.string.open, R.string.close)
        navDrawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        findViewById<ImageView>(R.id.btnHamburgerMenu).setOnClickListener {
            navDrawerLayout.openDrawer(navigationView)
        }

        navigationView.setNavigationItemSelectedListener {
            when(it.itemId){
                R.id.home -> {
                    navDrawerLayout.closeDrawer(navigationView)
                }
                R.id.programma -> {
                    startActivity(Intent(this, IlTuoEventoActivity::class.java))
                }
                R.id.food_exp -> {
                    startActivity(Intent(this, FoodExperienceActivity::class.java))
                }
                R.id.territory -> {
                    startActivity(Intent(this, TerritorioActivity::class.java))
                }
                R.id.audi_exp -> {
                    startActivity(Intent(this, AudiDriveExperienceActivity::class.java))
                }
                R.id.sondaggio -> {
                    //startActivity(Intent(this, PublicEventsActivity::class.java))
                }
                R.id.coupon -> {
                    //startActivity(Intent(this, PublicEventsActivity::class.java))
                }
                R.id.info -> {
                    //startActivity(Intent(this, PublicEventsActivity::class.java))
                }
                R.id.impostacioni -> {
                    //startActivity(Intent(this, PublicEventsActivity::class.java))
                }
            }

            true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)){
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    fun populateData() {
        val reconnectToNatureLayout = findViewById<LinearLayout>(R.id.reconnect_to_nature_layout)
        val reconnectToNatureImage = findViewById<ImageView>(R.id.reconnect_to_nature_image)
        val reconnectToNatureTitle = findViewById<TextView>(R.id.reconnect_to_nature_title)
        val reconnectToNatureSubtitle = findViewById<TextView>(R.id.reconnect_to_nature_subtitle)

        val foodExperienceLayout = findViewById<LinearLayout>(R.id.food_experience_layout)
        val foodExperienceImage = findViewById<ImageView>(R.id.food_experience_image)
        val foodExperienceTitle = findViewById<TextView>(R.id.food_experience_title)
        val foodExperienceSubtitle = findViewById<TextView>(R.id.food_experience_subtitle)

        val luoghiETerritorioLayout = findViewById<LinearLayout>(R.id.luoghi_e_territorio_layout)
        val luoghiETerritorioImage = findViewById<ImageView>(R.id.luoghi_e_territorio_image)
        val luoghiETerritorioTitle = findViewById<TextView>(R.id.luoghi_e_territorio_title)
        val luoghiETerritorioSubtitle = findViewById<TextView>(R.id.luoghi_e_territorio_subtitle)

        val audiDrivingExperienceLayout = findViewById<LinearLayout>(R.id.audi_driving_experience_layout)
        val audiDrivingExperienceImage = findViewById<ImageView>(R.id.audi_driving_experience_image)
        val audiDrivingExperienceTitle = findViewById<TextView>(R.id.audi_driving_experience_title)
        val audiDrivingExperienceSubtitle = findViewById<TextView>(R.id.audi_driving_experience_subtitle)

        val infoAndContattiLayout = findViewById<LinearLayout>(R.id.info_and_contatti_layout)
        val infoAndContattiImage = findViewById<ImageView>(R.id.info_and_contatti_image)
        val infoAndContattiTitle = findViewById<TextView>(R.id.info_and_contatti_title)

        val sondaggioLayout = findViewById<LinearLayout>(R.id.sondaggio_layout)
        val sondaggioImage = findViewById<ImageView>(R.id.sondaggio_image)
        val sondaggioTitle = findViewById<TextView>(R.id.sondaggio_title)
        val sondaggioSubtitle = findViewById<TextView>(R.id.sondaggio_subtitle)

        val eventiAudiLayout = findViewById<LinearLayout>(R.id.eventi_audi_layout)
        val eventiAudiImage = findViewById<ImageView>(R.id.eventi_audi_image)
        val eventiAudiTitle = findViewById<TextView>(R.id.eventi_audi_title)
        val eventiAudiSubtitle = findViewById<TextView>(R.id.eventi_audi_subtitle)

        val ilTuoEventoLayout = findViewById<LinearLayout>(R.id.il_tuo_evento_layout)
        val ilTuoEventoImage = findViewById<ImageView>(R.id.il_tuo_evento_image)
        val ilTuoEventoTitle = findViewById<TextView>(R.id.il_tuo_evento_title)
        val ilTuoEventoSubtitle = findViewById<TextView>(R.id.il_tuo_evento_subtitle)

        val gammaAudiLayout = findViewById<LinearLayout>(R.id.gamma_audi_layout)
        val gammaAudiImage = findViewById<ImageView>(R.id.gamma_audi_image)
        val gammaAudiTitle = findViewById<TextView>(R.id.gamma_audi_title)
        val gammaAudiSubtitle = findViewById<TextView>(R.id.gamma_audi_subtitle)

        reconnectToNatureTitle.text = "Reconnect to nature"
        reconnectToNatureSubtitle.text = "con Hervé Barmasse"
        global.utils.DownloadImage(reconnectToNatureImage).execute(global.testPremiumEventData[0].image.get("href") as String)

        foodExperienceTitle.text = global.langs.get("SIDE_MENU_PREMIUM_FOOD_EXPERIENCE")
        foodExperienceSubtitle.text = "Cosa mangiare"
        global.utils.DownloadImage(foodExperienceImage).execute(global.testFoodExperienceData[0].foodImage.get("href") as String)

        luoghiETerritorioTitle.text = global.langs.get("HOME_PLACES_TITLE")
        luoghiETerritorioSubtitle.text = global.langs.get("HOME_PLACES_SUBTITLE")
        global.utils.DownloadImage(luoghiETerritorioImage).execute(global.testPlacesAndTerritoryData[0].imagePlace.href)

        audiDrivingExperienceTitle.text = global.langs.get("HOME_ADE_SUBTITLE")
        audiDrivingExperienceSubtitle.text = "Scopri il programma"
        global.utils.DownloadImage(audiDrivingExperienceImage).execute(global.testAudiDrivingExperienceData[0].imageAde.href)

        infoAndContattiTitle.text = "Info & Contatti"
        global.utils.DownloadImage(infoAndContattiImage).execute("https://www.wendecar.it/media/8d8b61eebc5b56b/promo_audi_banner-hp.jpg")

        sondaggioTitle.text = global.langs.get("SURVEY_APPBAR_TITLE")
        sondaggioSubtitle.text = global.langs.get("SURVEY_BOTTOM_HEADER")
        global.utils.DownloadImage(sondaggioImage).execute("https://www.vestilanatura.it/wp-content/uploads/2020/10/sondaggio-pensi-alla-sostenibilita-di-un-prodotto.jpg")

        eventiAudiTitle.text = global.langs.get("HOME_GUEST_EVENT_LIST")
        eventiAudiSubtitle.text = global.langs.get("HOME_GUEST_EVENT_LIST_BUTTON")
        eventiAudiImage.setImageResource(R.drawable.eventi_home_guest)

        ilTuoEventoTitle.text = global.langs.get("HOME_GUEST_COUPON_LOGIN")
        ilTuoEventoSubtitle.text = "Inserisci coupon"
        ilTuoEventoImage.setImageResource(R.drawable.evento_coupon_home_guest)

        gammaAudiTitle.text = global.langs.get("HOME_GUEST_AUDI_RANGE")
        gammaAudiSubtitle.text = global.langs.get("HOME_GUEST_AUDI_RANGE_BUTTON")
        gammaAudiImage.setImageResource(R.drawable.gamma_audi_home)
    }
}