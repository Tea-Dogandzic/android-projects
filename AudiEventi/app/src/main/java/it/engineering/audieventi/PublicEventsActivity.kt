package it.engineering.audieventi

import adapter.PublicEventAdapter
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.navigation.NavigationView
import objects.global
import utils.CustomDialog
import utils.TestDataGenerator

class PublicEventsActivity : AppCompatActivity() {
    lateinit var toggle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_public_events)

        val publicEventsRecyclerView = findViewById<RecyclerView>(R.id.publicEventsRecyclerView)
        global.utils.setAdapter(PublicEventAdapter(global.testPublicEventData) as RecyclerView.Adapter<RecyclerView.ViewHolder>, publicEventsRecyclerView, this)

        val navDrawerLayout = findViewById<DrawerLayout>(R.id.drawerMenuLayout)
        val navigationView = findViewById<NavigationView>(R.id.navigationView)

        toggle = ActionBarDrawerToggle(this, navDrawerLayout, R.string.open, R.string.close)
        navDrawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        findViewById<ImageView>(R.id.btnHamburgerMenu).setOnClickListener {
            navDrawerLayout.openDrawer(navigationView)
        }

        navigationView.setNavigationItemSelectedListener {
            when(it.itemId){
                R.id.home -> {
                    startActivity(Intent(this, HomeGuestActivity::class.java))
                }
                R.id.calendar -> {
                    navDrawerLayout.closeDrawer(navigationView)
                }
                R.id.coupon -> {
                    navDrawerLayout.closeDrawer(navigationView)
                    global.showDialog = true
                    startActivity(Intent(this, HomeGuestActivity::class.java))
                }
            }

            true
        }

        findViewById<LinearLayout>(R.id.startSettingsActivity).setOnClickListener {
            startActivity(Intent(this, SettingsActivity::class.java))
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)){
            return true
        }

        return super.onOptionsItemSelected(item)
    }
}