package it.engineering.audieventi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import objects.global
import utils.CustomDialog

class SettingsActivity : AppCompatActivity() {
    lateinit var toggle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        /* HAMBURGER MENU */

        val navDrawerLayout = findViewById<DrawerLayout>(R.id.drawerMenuLayout)
        val navigationView = findViewById<NavigationView>(R.id.navigationView)

        toggle = ActionBarDrawerToggle(this, navDrawerLayout, R.string.open, R.string.close)
        navDrawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        findViewById<ImageView>(R.id.btnHamburgerMenu).setOnClickListener {
            navDrawerLayout.openDrawer(navigationView)
        }

        navigationView.setNavigationItemSelectedListener {
            when(it.itemId){
                R.id.home -> {
                    startActivity(Intent(this, HomeGuestActivity::class.java))
                }
                R.id.calendar -> {
                    startActivity(Intent(this, PublicEventsActivity::class.java))
                }
                R.id.coupon -> {
                    navDrawerLayout.closeDrawer(navigationView)
                    global.showDialog = true
                    startActivity(Intent(this, HomeGuestActivity::class.java))
                }
            }

            true
        }

        findViewById<LinearLayout>(R.id.startSettingsActivity).setOnClickListener {
            navDrawerLayout.closeDrawer(navigationView)
        }
    }
}