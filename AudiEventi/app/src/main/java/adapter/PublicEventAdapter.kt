package adapter

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import it.engineering.audieventi.R
import models.PublicEvent
import java.net.URL

class PublicEventAdapter(private val publicEvents: ArrayList<PublicEvent>) : RecyclerView.Adapter<CustomMoviesViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomMoviesViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val recycleViewRow = layoutInflater.inflate(R.layout.public_event_cell, parent, false)

        return CustomMoviesViewHolder(recycleViewRow)
    }

    override fun onBindViewHolder(holder: CustomMoviesViewHolder, position: Int) {
        holder.view.findViewById<TextView>(R.id.publicEventTitle).text = publicEvents[position].title

        val calendarioEventiSubtitle = publicEvents[position].description.get("value")!!.drop(3).dropLast(4)
        holder.view.findViewById<TextView>(R.id.publicEventSubtitle).text = calendarioEventiSubtitle

        val imageUrl = publicEvents[position].backgroundImage.get("href").toString()

        DownloadImage(holder.view.findViewById(R.id.publicEventImg)).execute(imageUrl)

    }

    override fun getItemCount(): Int {
        return publicEvents.count()
    }

    inner class DownloadImage(val imageHolder: ImageView): AsyncTask<String, Void, Bitmap?>(){

        override fun doInBackground(vararg params: String?): Bitmap? {
            var image: Bitmap? = null
            val imageUrl = params[0]

            try {
                val imageDownloaded = URL(imageUrl).openStream()
                image = BitmapFactory.decodeStream(imageDownloaded)
            } catch (e:Exception){
                e.printStackTrace()
            }

            return image
        }

        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)

            imageHolder.setImageBitmap(result)
        }
    }
}

class CustomMoviesViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}