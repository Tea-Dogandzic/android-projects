package utils

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import it.engineering.audieventi.HomeCouponActivity
import it.engineering.audieventi.HomeGuestActivity
import it.engineering.audieventi.R
import objects.global

class CustomDialog: DialogFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val layout = inflater.inflate(R.layout.insert_coupon_dialog, container, false)

        val textViewGenerateCoupon = layout.findViewById<TextView>(R.id.generateCoupon)
        val submitCoupon = layout.findViewById<Button>(R.id.submitCoupon)
        val dismissDialog = layout.findViewById<ImageView>(R.id.dismissDialog)
        val confirmCoupon = layout.findViewById<Button>(R.id.confirmCoupon)

        submitCoupon.setOnClickListener {
            val coupon = generateCoupon()

            textViewGenerateCoupon.text = coupon
            submitCoupon.visibility = View.GONE
            confirmCoupon.visibility = View.VISIBLE
        }

        confirmCoupon.setOnClickListener {
            dialog!!.dismiss()
            global.premiumUser = true
            startActivity(Intent(context, HomeCouponActivity::class.java), null)
        }

        dismissDialog.setOnClickListener {
            dialog!!.dismiss()
        }

        return layout
    }

    private fun generateCoupon(): String {
        val randomNumber = (2000000..3000000).random()

        return "Audi $randomNumber"
    }
}