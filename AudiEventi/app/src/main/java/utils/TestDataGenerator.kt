package utils

import android.content.Context
import android.util.Log
import models.*
import objects.global
import org.json.JSONArray
import java.io.IOException
import java.io.InputStream
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class TestDataGenerator {
//    val testPublicEventData: ArrayList<PublicEvent> = arrayListOf()
//    val testPremiumEventData: ArrayList<PremiumEvent> = arrayListOf()
//    val testFoodExperienceData: ArrayList<FoodExperience> = arrayListOf()
//    val testAudiDrivingExperienceData: ArrayList<AudiDrivingExperience> = arrayListOf()
//    val testPlacesAndTerritoryData: ArrayList<Place> = arrayListOf()

    fun readPublicEventsData(context: Context) {
        val json: String?

        try {
            val InputStream: InputStream = context.assets.open("public_events.json")
            json = InputStream.bufferedReader().use {
                it.readText()
            }

            val jsonArray = JSONArray(json)
            jsonArray.let {
                (0 until it.length()).forEach {
                    val jsonObject = jsonArray.getJSONObject(it)

                    val id = jsonObject.getString("id")
                    val title = jsonObject.getString("title")
                    val description = jsonObject.getJSONObject("description") // TO DO
                    val format = description.getString("format")
                    val value = description.getString("value")
                    val eventDate = jsonObject.getString("eventDate")
                    val linkMyAudi = jsonObject.getJSONObject("linkMyAudi") // TO DO
                    val uri = linkMyAudi.getString("uri")
                    val linkMyAudiTitle = linkMyAudi.getString("title")
                    val options = linkMyAudi.getJSONArray("options")
                    val priority = jsonObject.getInt("priority")
                    val status = jsonObject.getString("status")
                    val backgroundImage = jsonObject.getJSONObject("backgroundImage") // TO DO
                    val backgroundImageId = backgroundImage.getString("id")
                    val href = backgroundImage.getString("href")
                    val meta = backgroundImage.getJSONObject("meta")
                    val alt = meta.getString("alt")
                    val metaTitle = meta.getString("title")
                    val width = meta.getString("width")
                    val height = meta.getString("height")

                    val descriptionHashMap: HashMap<String, String> = hashMapOf()
                    descriptionHashMap.put("format", format)
                    descriptionHashMap.put("value", value)

                    val linkMyAudiHashMap: HashMap<String, Any> = hashMapOf()
                    linkMyAudiHashMap.put("uri", uri)
                    linkMyAudiHashMap.put("title", linkMyAudiTitle)
                    linkMyAudiHashMap.put("options", options)

                    val backgroundImageHashMap: HashMap<String, Any> = hashMapOf()
                    backgroundImageHashMap.put("id", backgroundImageId)
                    backgroundImageHashMap.put("href", href)
                    backgroundImageHashMap.put("meta", Meta(alt, metaTitle, width, height))

                    global.testPublicEventData.add(PublicEvent(id, title, descriptionHashMap, eventDate, linkMyAudiHashMap, priority, status, backgroundImageHashMap))
                }
            }
        } catch (e: IOException){
            Log.d("JSON Exception", e.toString())
        }

        for (e in global.testPublicEventData) {
            Log.d("PUBLICEVENT", e.toString())
        }
    }

    fun readPremiumEventsData(context: Context) {
        val json: String?

        try {
            val InputStream: InputStream = context.assets.open("premium_events.json")
            json = InputStream.bufferedReader().use {
                it.readText()
            }

            val jsonArray = JSONArray(json)
            jsonArray.let {
                (0 until it.length()).forEach {
                    val jsonObject = jsonArray.getJSONObject(it)

                    val id = jsonObject.getString("id")

                    val title = jsonObject.getString("title")

                    val description = jsonObject.getJSONObject("description") // TO DO
                    val value = description.getString("value")
                    val format = description.getString("format")
                    val processed = description.getString("processed")

                    val headerPremium = jsonObject.getString("headerPremium")

                    val linkMyAudiPremium = jsonObject.getJSONArray("linkMyAudiPremium") // TO DO
                    val linkMyAudiPremiumArray: ArrayList<Link> = arrayListOf()
                    linkMyAudiPremium.let {
                        (0 until it.length()).forEach {
                            val linkMyAudiJsonObject = linkMyAudiPremium.getJSONObject(it)

                            val uri = linkMyAudiJsonObject.getString("uri")
                            val linkMyAudiTitle = linkMyAudiJsonObject.getString("title")
                            val options = linkMyAudiJsonObject.getJSONArray("options")

                            linkMyAudiPremiumArray.add(
                                Link(
                                    uri,
                                    linkMyAudiTitle,
                                    options
                                )
                            )
                        }
                    }

                    val programmeNote = jsonObject.getJSONObject("programmeNote")
                    val programmeNoteValue = programmeNote.getString("value")
                    val programmeNoteFormat = programmeNote.getString("format")
                    val programmeNotepPocessed = programmeNote.getString("processed")

                    val detailedPlan = jsonObject.getJSONArray("detailedPlan") // TO DO
                    val detailedPlanArray: ArrayList<HashMap<String, Any>> = arrayListOf()
                    detailedPlan.let {
                        (0 until it.length()).forEach {
                            val detailedPlanJsonObject = detailedPlan.getJSONObject(it)

                            val day = detailedPlanJsonObject.getString("day")
                            val activities = detailedPlanJsonObject.getJSONArray("activities")
                            val activitiesArray: ArrayList<HashMap<String, String>> = arrayListOf()

                            activities.let {
                                (0 until it.length()).forEach {
                                    val activityJsonObject = activities.getJSONObject(it)

                                    val start = activityJsonObject.getString("start")
                                    val end = activityJsonObject.getString("end")
                                    val activity = activityJsonObject.getString("activity")

                                    val activityHashMap: HashMap<String, String> = hashMapOf()
                                    activityHashMap.put("start", start)
                                    activityHashMap.put("end", end)
                                    activityHashMap.put("activity", activity)

                                    activitiesArray.add(activityHashMap)
                                }
                            }

                            val detailedPlanHashMap: HashMap<String, Any> = hashMapOf()
                            detailedPlanHashMap.put("day", day)
                            detailedPlanHashMap.put("activities", activitiesArray)

                            detailedPlanArray.add(detailedPlanHashMap)
                        }
                    }

                    val subtitle = jsonObject.getString("subtitle")

                    val image = jsonObject.getJSONObject("image") // TO DO
                    val imageId = image.getString("id")
                    val href = image.getString("href")
                    val meta = image.getJSONObject("meta")
                    val alt = meta.getString("alt")
                    val metaTitle = meta.getString("title")
                    val width = meta.getString("width")
                    val height = meta.getString("height")

                    val descriptionHashMap: HashMap<String, String> = hashMapOf()
                    descriptionHashMap.put("value", value)
                    descriptionHashMap.put("format", format)
                    descriptionHashMap.put("processed", processed)

                    val programmeNoteHashMap: HashMap<String, String> = hashMapOf()
                    programmeNoteHashMap.put("value", programmeNoteValue)
                    programmeNoteHashMap.put("format", programmeNoteFormat)
                    programmeNoteHashMap.put("processed", programmeNotepPocessed)

                    val imageHashMap: HashMap<String, Any> = hashMapOf()
                    imageHashMap.put("id", imageId)
                    imageHashMap.put("href", href)
                    imageHashMap.put("meta", Meta(alt, metaTitle, width, height))

                    global.testPremiumEventData.add(PremiumEvent(id, title, descriptionHashMap, headerPremium, linkMyAudiPremiumArray, programmeNoteHashMap, detailedPlanArray, subtitle, imageHashMap))
                }
            }
        } catch (e: IOException){
            Log.d("JSON Exception", e.toString())
        }

        for (e in global.testPremiumEventData) {
            Log.d("PREMIUMEVENT", e.toString())
        }
    }

    fun readFoodExperienceData(context: Context) {
        val json: String?

        try {
            val InputStream: InputStream = context.assets.open("food_experience.json")
            json = InputStream.bufferedReader().use {
                it.readText()
            }

            val jsonArray = JSONArray(json)
            jsonArray.let {
                (0 until it.length()).forEach {
                    val jsonObject = jsonArray.getJSONObject(it)

                    val id = jsonObject.getString("id")

                    val title = jsonObject.getString("title")

                    val header = jsonObject.getString("header")

                    val subtitleFood = jsonObject.getJSONObject("subtitleFood") // TO DO
                    val value = subtitleFood.getString("value")
                    val format = subtitleFood.getString("format")
                    val processed = subtitleFood.getString("processed")

                    val foodImage = jsonObject.getJSONObject("foodImage") // TO DO
                    val foodImageId = foodImage.getString("id")
                    val href = foodImage.getString("href")
                    val meta = foodImage.getJSONObject("meta")
                    val alt = meta.getString("alt")
                    val metaTitle = meta.getString("title")
                    val width = meta.getString("width")
                    val height = meta.getString("height")

                    val programmeExperience = jsonObject.getJSONArray("programmeExperience")
                    val programmeExperienceArray: ArrayList<FoodExperienceDay> = arrayListOf()
                    programmeExperience.let {
                        (0 until it.length()).forEach {
                            val programmeExperienceJsonObject = programmeExperience.getJSONObject(it)

                            val day = programmeExperienceJsonObject.getString("day")
                            val experience = programmeExperienceJsonObject.getJSONObject("experience")
                            val startTime = experience.getString("startTime")
                            val type = experience.getString("type")
                            val activity = experience.getString("activity")
                            val place = experience.getString("place")
                            val description = experience.getString("description")
                            val food = experience.getString("food")
                            val allergens = experience.getString("allergens")

                            programmeExperienceArray.add(FoodExperienceDay(day, Experience(startTime, type, activity, place, description, food, allergens)))
                        }
                    }

                    val subtitleFoodHashMap: HashMap<String, String> = hashMapOf()
                    subtitleFoodHashMap.put("value", value)
                    subtitleFoodHashMap.put("format", format)
                    subtitleFoodHashMap.put("processed", processed)

                    val foodImageHashMap: HashMap<String, Any> = hashMapOf()
                    foodImageHashMap.put("id", foodImageId)
                    foodImageHashMap.put("href", href)
                    foodImageHashMap.put("meta", Meta(alt, metaTitle, width, height))

                    global.testFoodExperienceData.add(FoodExperience(id, title, header, subtitleFoodHashMap, foodImageHashMap, programmeExperienceArray))
                }
            }
        } catch (e: IOException){
            Log.d("JSON Exception", e.toString())
        }

        for (e in global.testFoodExperienceData) {
            Log.d("FOODEXPERIENCE", e.toString())
        }
    }

    fun readAudiDrivingExperienceData(context: Context) {
        val json: String?

        try {
            val InputStream: InputStream = context.assets.open("audi_driving_experience.json")
            json = InputStream.bufferedReader().use {
                it.readText()
            }

            val jsonArray = JSONArray(json)
            jsonArray.let {
                (0 until it.length()).forEach {
                    val jsonObject = jsonArray.getJSONObject(it)

                    val data = jsonObject.getJSONObject("data")

                    val id = jsonObject.getString("id")

                    val freeText = jsonObject.getString("freeText")

                    val subtitleAde = jsonObject.getString("subtitleAde")

                    val titleAde = jsonObject.getString("titleAde")

                    val carouselAde = jsonObject.getJSONArray("carouselAde")
                    val carouselAdeArray: ArrayList<BackgroundImage> = arrayListOf()
                    carouselAde.let {
                        (0 until it.length()).forEach {
                            val carouselAdeJsonObject = carouselAde.getJSONObject(it)

                            val id = carouselAdeJsonObject.getString("id")
                            val href = carouselAdeJsonObject.getString("href")
                            val meta = carouselAdeJsonObject.getJSONObject("meta")
                            val alt = meta.getString("alt")
                            val metaTitle = meta.getString("title")
                            val width = meta.getString("width")
                            val height = meta.getString("height")

                            carouselAdeArray.add(BackgroundImage(id, href, Meta(alt, metaTitle, width, height)))
                        }
                    }

                    val imageAde = jsonObject.getJSONObject("imageAde")
                    val imageAdeId = imageAde.getString("id")
                    val href = imageAde.getString("href")
                    val meta = imageAde.getJSONObject("meta")
                    val alt = meta.getString("alt")
                    val metaTitle = meta.getString("title")
                    val width = meta.getString("width")
                    val height = meta.getString("height")

                    global.testAudiDrivingExperienceData.add(AudiDrivingExperience(data, id, freeText, subtitleAde, titleAde, carouselAdeArray, BackgroundImage(imageAdeId, href, Meta(alt, metaTitle, width, height))))
                }
            }
        } catch (e: IOException){
            Log.d("JSON Exception", e.toString())
        }

        for (e in global.testAudiDrivingExperienceData) {
            Log.d("ADE", e.toString())
        }
    }

    fun readPlacesAndTerritoryData(context: Context) {
        val json: String?

        try {
            val InputStream: InputStream = context.assets.open("places_and_territory.json")
            json = InputStream.bufferedReader().use {
                it.readText()
            }

            val jsonArray = JSONArray(json)
            jsonArray.let {
                (0 until it.length()).forEach {
                    val jsonObject = jsonArray.getJSONObject(it)

                    val id = jsonObject.getString("id")

                    val title = jsonObject.getString("title")

                    val description = jsonObject.getString("description")

                    val placeSubtitle = jsonObject.getString("placeSubtitle")

                    val placeTitle = jsonObject.getString("placeTitle")

                    val carouselPlace = jsonObject.getJSONArray("carouselPlace")
                    val carouselPlaceArray: ArrayList<BackgroundImage> = arrayListOf()
                    carouselPlace.let {
                        (0 until it.length()).forEach {
                            val carouselPlaceJsonObject = carouselPlace.getJSONObject(it)

                            val id = carouselPlaceJsonObject.getString("id")
                            val href = carouselPlaceJsonObject.getString("href")
                            val meta = carouselPlaceJsonObject.getJSONObject("meta")
                            val alt = meta.getString("alt")
                            val metaTitle = meta.getString("title")
                            val width = meta.getString("width")
                            val height = meta.getString("height")

                            carouselPlaceArray.add(BackgroundImage(id, href, Meta(alt, metaTitle, width, height)))
                        }
                    }

                    val imagePlace = jsonObject.getJSONObject("imagePlace")
                    val imagePlaceId = imagePlace.getString("id")
                    val href = imagePlace.getString("href")
                    val meta = imagePlace.getJSONObject("meta")
                    val alt = meta.getString("alt")
                    val metaTitle = meta.getString("title")
                    val width = meta.getString("width")
                    val height = meta.getString("height")

                    global.testPlacesAndTerritoryData.add(Place(id, title, description, placeSubtitle, placeTitle, carouselPlaceArray, BackgroundImage(imagePlaceId, href, Meta(alt, title, width, height))))
                }
            }
        } catch (e: IOException){
            Log.d("JSON Exception", e.toString())
        }

        for (e in global.testPlacesAndTerritoryData) {
            Log.d("PLACES", e.toString())
        }
    }
}