package utils

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import it.engineering.audieventi.HomeCouponActivity
import it.engineering.audieventi.HomeGuestActivity
import it.engineering.audieventi.R
import objects.global

class ListaAllergeniDialog: DialogFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val layout = inflater.inflate(R.layout.lista_allergeni, container, false)

        val listaAllergeni = layout.findViewById<TextView>(R.id.listaAllergeni)
        val chiudiBtn = layout.findViewById<Button>(R.id.chiudiBtn)

        chiudiBtn.setOnClickListener {
            dialog!!.dismiss()
        }

        val allergeniString = global.testFoodExperienceData[0].programmeExperience[1].experience.allergens
        val allergeniSeparated: List<String> = allergeniString.split(";").map { it -> it.trim() }

        var preparedText = ""
        for (a in allergeniSeparated) preparedText += a + "\n"
        listaAllergeni.text = preparedText

        return layout
    }
}