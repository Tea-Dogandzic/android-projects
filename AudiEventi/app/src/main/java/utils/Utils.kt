package utils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import objects.global
import org.json.JSONArray
import org.json.JSONObject
import java.net.URL

class Utils {
     fun parseJSON(data: JSONObject?) {
        if (data != null) {
            val it = data.keys()
            while (it.hasNext()) {
                val key = it.next()
                try {
                    if (data[key] is JSONArray) {
                        val arry = data.getJSONArray(key)
                        val size = arry.length()
                        for (i in 0 until size) {
                            parseJSON(arry.getJSONObject(i))
                        }
                    } else if (data[key] is JSONObject) {
                        parseJSON(data.getJSONObject(key))
                    } else {
                        println(key + ":" + data.getString(key))
                        global.langs.put(key, data.getString(key))
                    }
                } catch (e: Throwable) {
                    try {
                        println(key + ":" + data.getString(key))
                    } catch (ee: Exception) {
                    }
                    e.printStackTrace()
                }
            }
        }
    }

    fun concatenateUrl(part1: String, part2: String):String {
        return part1 + part2
    }

    fun setAdapter(adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>, recyclerView: RecyclerView, activity: AppCompatActivity) {
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
    }

    inner class DownloadImage(val imageHolder: ImageView): AsyncTask<String, Void, Bitmap?>(){

        override fun doInBackground(vararg params: String?): Bitmap? {
            var image: Bitmap? = null
            val imageUrl = params[0]

            try {
                val imageDownloaded = URL(imageUrl).openStream()
                image = BitmapFactory.decodeStream(imageDownloaded)
            } catch (e:Exception){
                e.printStackTrace()
            }

            return image
        }

        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)

            imageHolder.setImageBitmap(result)

        }
    }
}