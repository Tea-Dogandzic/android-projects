package it.engineering.katalogautomobila

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import java.io.BufferedReader
import java.io.InputStreamReader

class MainActivity : AppCompatActivity() {
    val objectArray : ArrayList<List<String>> = arrayListOf()
    var naziviAtributa: ArrayList<String> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //checkLogin()

        val btnSeeAllModels = findViewById<Button>(R.id.btnAllModels)

        btnSeeAllModels.setOnClickListener {
            startActivity(Intent(this, MarkeRecyclerActivity::class.java))
        }

        parseCSV()
        global.data = objectArray
        global.dataAttrs = naziviAtributa

        Log.d("Lista", objectArray.toString())
        Log.d("ATTR", naziviAtributa.toString())
    }

    fun checkLogin() {
        if (!global.user.equals(global.inputUser) || !global.pass.equals(global.inputPass)) {
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }

    private fun parseCSV() {
        var linija : String?

        val otvoriCSV = InputStreamReader(assets.open("modelExcelNew.csv"))
        val procitajLiniju = BufferedReader(otvoriCSV)

        naziviAtributa = procitajLiniju.readLine().split(";") as ArrayList<String>

        while (procitajLiniju.readLine().also {
                linija = it
            } != null) {

            val red : List<String> = linija!!.split(";")

            if (red[0].isEmpty()) {
                Log.d("CSV", "Red je prazan!")
            } else {
                objectArray.add(red)
            }
        }
    }
}