package it.engineering.katalogautomobila

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

class ModeliRecyclerActivity : AppCompatActivity() {

    var modeliMarkeArray : ArrayList<List<String>> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_modeli_recycler)

        var slikaMarke = findViewById<ImageView>(R.id.prikazSlike)
        slikaMarke.setBackgroundResource(global.markaZaPrikaz.image)

        modeliMarkeArray = global.data.filter { it -> it[0] == global.markaZaPrikaz.title } as ArrayList<List<String>>

        val adapter = modelRecyclerAdapter(modeliMarkeArray)
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerViewModeli)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false)
    }
}

