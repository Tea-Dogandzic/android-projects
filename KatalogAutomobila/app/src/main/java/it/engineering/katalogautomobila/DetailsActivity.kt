package it.engineering.katalogautomobila

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

class DetailsActivity : AppCompatActivity() {

    var model: ArrayList<String> = arrayListOf()
    var indexSlike: Int = -1
    var showMore: Boolean = false;

    var prikaz1: String = ""
    var prikaz2: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        val slikaMarke = findViewById<ImageView>(R.id.slikaModela)
        val prikazCene = findViewById<TextView>(R.id.prikazCene)
        var ispis1 = findViewById<TextView>(R.id.textView1)
        var ispis2 = findViewById<TextView>(R.id.textView2)
        var show = findViewById<TextView>(R.id.showText)

        model = intent.getSerializableExtra("MODEL") as ArrayList<String>
        indexSlike = intent.getSerializableExtra("INDEXSLIKE") as Int

        slikaMarke.setImageResource(global.modelImages[indexSlike])
        prikazCene.text = global.convertPrice(model[4]) + " $"

        loadDetails()

        ispis1.text = prikaz1
        ispis2.text = prikaz2

        show.setOnClickListener {
            if(!showMore) {
                ispis2.visibility = View.VISIBLE
                show.text = getString(R.string.label_show_less)
            } else {
                ispis2.visibility = View.GONE
                show.text = getString(R.string.label_show_more)
            }
            showMore = !showMore
        }
    }

    private fun loadDetails() {
        for (a in 0..9) {
            prikaz1 += global.dataAttrs[a] + " -> " + model[a] + "\n"
        }

        for (a in 10..global.dataAttrs.count() - 1) {
            prikaz2 += global.dataAttrs[a] + " -> " + model[a] + "\n"
        }
    }
}