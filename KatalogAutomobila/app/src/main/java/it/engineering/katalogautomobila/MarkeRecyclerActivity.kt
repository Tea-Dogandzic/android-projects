package it.engineering.katalogautomobila

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import it.engineering.katalogautomobila.models.Marka

class MarkeRecyclerActivity : AppCompatActivity() {

    val markeArray : ArrayList<Marka> = arrayListOf()
    var searchArray : ArrayList<List<String>> = arrayListOf()

    var searchWords : ArrayList<String> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_marke_recycler)

        createDataSource()

        displayRecycler(markeArray)

        val textSearh = findViewById<EditText>(R.id.pretraga)
        val btnSearh = findViewById<Button>(R.id.btnPretraga)

        btnSearh.setOnClickListener {
            var searchedText: String = textSearh.text.toString()

            searchArray = arrayListOf()

            for (m in global.data) {
                val searchTerm = m[0] + m[1]
                if (searchTerm.replace(" ", "").contains(searchedText.replace(" ", ""), true) && !searchArray.contains(m)) {
                    searchArray.add(m)
                }
            }

            val adapter = modelRecyclerAdapter(searchArray)
            val recyclerView = findViewById<RecyclerView>(R.id.recyclerViewMarke)
            recyclerView.adapter = adapter
            recyclerView.layoutManager = GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false)
        }
    }

    private fun displayRecycler(data: ArrayList<Marka>) {
        val adapter = markeRecyclerAdapter(data)
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerViewMarke)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false)
    }

    private fun createDataSource() {
        markeArray.add(
            Marka(
                "Alfa Romeo",
                R.drawable.alfa_romeo
            )
        )
        markeArray.add(
            Marka(
                "Audi",
                R.drawable.audi
            )
        )
        markeArray.add(
            Marka(
                "Citroen",
                R.drawable.citroen
            )
        )
        markeArray.add(
            Marka(
                "Dacia",
                R.drawable.dacia
            )
        )
        markeArray.add(
            Marka(
                "Fiat",
                R.drawable.fiat
            )
        )
        markeArray.add(
            Marka(
                "Ford",
                R.drawable.ford
            )
        )
        markeArray.add(
            Marka(
                "Honda",
                R.drawable.honda
            )
        )
        markeArray.add(
            Marka(
                "Hyundai",
                R.drawable.hyundai
            )
        )
        markeArray.add(
            Marka(
                "Infiniti",
                R.drawable.infiniti
            )
        )
        markeArray.add(
            Marka(
                "Isuzu",
                R.drawable.isuzu
            )
        )
        markeArray.add(
            Marka(
                "Jeep",
                R.drawable.jeep
            )
        )
        markeArray.add(
            Marka(
                "Lada",
                R.drawable.lada
            )
        )
        markeArray.add(
            Marka(
                "Mazda",
                R.drawable.mazda
            )
        )
        markeArray.add(
            Marka(
                "Mercedes Benz",
                R.drawable.mercedes_benz
            )
        )
        markeArray.add(
            Marka(
                "Mini",
                R.drawable.mini
            )
        )
        markeArray.add(
            Marka(
                "Mitsubishi",
                R.drawable.mitsubishi
            )
        )
        markeArray.add(
            Marka(
                "Nissan",
                R.drawable.nissan
            )
        )
        markeArray.add(
            Marka(
                "Opel",
                R.drawable.opel
            )
        )
        markeArray.add(
            Marka(
                "Peugeot",
                R.drawable.peugeot
            )
        )
        markeArray.add(
            Marka(
                "Renault",
                R.drawable.renault
            )
        )
        markeArray.add(
            Marka(
                "Seat",
                R.drawable.seat
            )
        )
        markeArray.add(
            Marka(
                "Skoda",
                R.drawable.skoda
            )
        )
//        markeArray.add(
//            Marka(
//                "Smart",
//                R.drawable.smart
//            )
//        )
        markeArray.add(
            Marka(
                "Subaru",
                R.drawable.subaru
            )
        )
        markeArray.add(
            Marka(
                "Suzuki",
                R.drawable.suzuki
            )
        )
        markeArray.add(
            Marka(
                "Volkswagen",
                R.drawable.volkswagen
            )
        )
        markeArray.add(
            Marka(
                "Volvo",
                R.drawable.volvo
            )
        )
    }
}