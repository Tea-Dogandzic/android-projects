package it.engineering.navigacijafragmenti

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_drugi.view.*

class DrugiFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_drugi, container, false)

        activity?.setTitle(getString(R.string.naslov2))

        view.drugiText.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_drugiFragment_to_treciFragment)
        }

        return view
    }
}