package it.engineering.navigacijafragmenti

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_prvi.view.*

class PrviFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_prvi, container, false)

        activity?.setTitle(getString(R.string.naslov1))

        view.prviText.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_prviFragment_to_drugiFragment)
        }

        return view
    }
}