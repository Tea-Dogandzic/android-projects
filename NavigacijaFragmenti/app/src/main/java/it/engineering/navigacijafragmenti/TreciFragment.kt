package it.engineering.navigacijafragmenti

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_prvi.view.*
import kotlinx.android.synthetic.main.fragment_treci.view.*

class TreciFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_treci, container, false)

        activity?.setTitle(getString(R.string.naslov3))

        view.treciText.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_treciFragment_to_cetvrtiFragment)
        }

        return view
    }
}