package it.engineering.radsatabelomobjekata

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.ViewGroup
import android.widget.TableRow
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.setPadding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    data class Person(val ime: String, val prezime: String, val godine: Int)

    var tableObjects: ArrayList<Person> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        createObjectsSourse()

        tableView.apply {
            isStretchAllColumns = true
        }

        addObjectToTable()

        btnDodaj.setOnClickListener {
            tableView.removeAllViews()
            tableObjects.add(Person(inputIme.text.toString(), inputPrezime.text.toString(), inputGodine.text.toString().toInt()))
            addObjectToTable()
        }

        btnUkloni.setOnClickListener {
            var counter = 0
            var removeIndex = -1

            tableView.removeAllViews()

            tableObjects.forEach {
                val upperCaps = it.ime.toUpperCase()
                if (upperCaps == inputIme.text.toString().toUpperCase()){
                    removeIndex = counter
                }
                counter += 1
            }

            if (removeIndex == -1){
                Toast.makeText(this, "Index nije dobar", Toast.LENGTH_SHORT).show()
            } else {
                tableObjects.removeAt(removeIndex)
            }

            addObjectToTable()
        }
    }

    private fun createObjectsSourse() {
        tableObjects.add(Person("Tea","Dogandzic", 22))
        tableObjects.add(Person("Eleonora","Dogandzic", 26))
        tableObjects.add(Person("Danijel","Bogicevic", 26))
    }


    fun addObjectToTable() {
        for (singleObject in tableObjects){
            val row = TableRow(this)
            row.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            row.gravity = Gravity.CENTER_HORIZONTAL

            for (count in 1..3) {
                val textView = TextView(this)
                textView.textSize = 22f
                textView.gravity = Gravity.CENTER_HORIZONTAL
                textView.setPadding(20)
                textView.apply {
                    if (count == 1) {
                        textView.text = singleObject.ime
                    } else if (count == 2) {
                        textView.text = singleObject.prezime
                    } else if (count == 3) {
                        textView.text = singleObject.godine.toString()
                    }

                }

                row.addView(textView)
            }

            tableView.addView(row)
        }
    }
}