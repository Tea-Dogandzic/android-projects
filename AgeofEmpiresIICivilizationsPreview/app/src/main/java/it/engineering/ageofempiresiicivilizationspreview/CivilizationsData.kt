package it.engineering.ageofempiresiicivilizationspreview

import android.content.Context
import android.util.Log
import org.json.JSONArray
import java.io.IOException
import java.io.InputStream

class CivilizationsData {
    val civilizations : ArrayList<Civilization> = arrayListOf()

    fun parseJSON(context: Context) {
        val json: String?

        try {
            val InputStream: InputStream = context.assets.open("Age of Empires II - civilizations.json")
            json = InputStream.bufferedReader().use {
                it.readText()
            }

            val jsonNiz = JSONArray(json)
            jsonNiz.let {
                (0 until it.length()).forEach {
                    val jsonObjekat = jsonNiz.getJSONObject(it)

                    val name = jsonObjekat.getString("name")
                    val expansion = jsonObjekat.getString("expansion")
                    val armyType = jsonObjekat.getString("army_type")
                    val teamBonus = jsonObjekat.getString("team_bonus")
                    val civilizationBonusJSONArray = jsonObjekat.getJSONArray("civilization_bonus")

                    val civilizationBonus: ArrayList<String> = arrayListOf()

                    civilizationBonusJSONArray.let {
                        (0 until it.length()).forEach {
                            val bonus = civilizationBonusJSONArray.getString(it)

                            civilizationBonus.add(bonus)
                        }
                    }

                    civilizations.add(Civilization(name, expansion, armyType, teamBonus, civilizationBonus))
                }
            }
        } catch (e: IOException){
            Log.d("JSON Exception", e.toString())
        }
    }
}