package it.engineering.pickers

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    val formatDatuma = SimpleDateFormat("dd-MM-YYYY", Locale.US)
    val formatVremena = SimpleDateFormat("HH:mm", Locale.ITALY)

    var jezik = ""
    var datum = ""
    var vreme = ""
    var sve = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val dugme = findViewById<TextView>(R.id.btnClickMe)

        dugme.setOnClickListener {
            showChangeLang()
        }
    }

    private fun showChangeLang() {
        val ispisJezika = findViewById<TextView>(R.id.prikazJezika)
        val nizJezika = arrayOf("Srpski", "Engleski", "Spanski", "Ruski")

        //Pravimo AlertDialog
        val pikerJezici = AlertDialog.Builder(this)
        pikerJezici.setTitle("Izaberite jezik...")
        pikerJezici.setSingleChoiceItems(nizJezika, -1) { dialog, jezik ->
            this.jezik = nizJezika[jezik]
            ispisJezika.text = nizJezika[jezik]
            dialog.dismiss()

            uzmiDatum()
        }

        //Uzmi AlertDialog i prikazi ga
        val prikazi = pikerJezici.create()
        prikazi.show()
    }

    private fun uzmiDatum() {
        val prikazDatuma = findViewById<TextView>(R.id.prikazDatuma)
        val sadasnjeVreme = Calendar.getInstance()
        val pikerDatum = DatePickerDialog(this, DatePickerDialog.OnDateSetListener{
                picker, year, month, dayOfMonth ->
            val sadasnjeVreme = Calendar.getInstance()
            sadasnjeVreme.set(Calendar.YEAR, year)
            sadasnjeVreme.set(Calendar.MONTH, month)
            sadasnjeVreme.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            //Instanca Calendar-a uzima trenutni datum i vreme
            val ispisDatuma = formatDatuma.format(sadasnjeVreme.time)
            datum = ispisDatuma
            prikazDatuma.text = ispisDatuma
            uzmiVreme()
        }, sadasnjeVreme.get(Calendar.YEAR), sadasnjeVreme.get(Calendar.MONTH), sadasnjeVreme.get(Calendar.DAY_OF_MONTH))

        pikerDatum.show()
    }

    private fun uzmiVreme() {
        val prikazVremena = findViewById<TextView>(R.id.prikazVremena)
        val prikazSvega = findViewById<TextView>(R.id.prikazSvega)
        val sadasnjeVreme = Calendar.getInstance()
        val pikerVreme = TimePickerDialog(this, TimePickerDialog.OnTimeSetListener{
                picker, hourOfDay, minute ->
            val sadasnjeVreme = Calendar.getInstance()
            sadasnjeVreme.set(Calendar.HOUR_OF_DAY, hourOfDay)
            sadasnjeVreme.set(Calendar.MINUTE, minute)
            val ispisVremena = formatVremena.format(sadasnjeVreme.time)
            prikazVremena.text = ispisVremena

            sve = jezik + " " + datum + " " + ispisVremena
            prikazSvega.text = sve
        }, sadasnjeVreme.get(Calendar.HOUR_OF_DAY), sadasnjeVreme.get(Calendar.MINUTE), true)

        pikerVreme.show()
    }
}