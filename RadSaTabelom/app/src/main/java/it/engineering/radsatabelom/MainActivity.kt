package it.engineering.radsatabelom

import android.graphics.Color
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup
import android.widget.TableRow
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.setPadding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var tableContent: ArrayList<String> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        createDatasource()

        addStringToTable()

        btnDodaj.setOnClickListener {
//            tableView.removeAllViews()
//            tableContent.add(inputText.text.toString())
//            addStringToTable()

            val row = TableRow(this)
            row.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            row.gravity = Gravity.CENTER_HORIZONTAL

            val textView = TextView(this)
            textView.textSize = 30f
            textView.setTextColor(Color.DKGRAY)
            textView.setTypeface(Typeface.DEFAULT_BOLD);
            textView.setBackgroundResource(R.color.grey)
            textView.gravity = Gravity.CENTER_HORIZONTAL
            textView.setPadding(20)
            textView.apply {
                textView.text = inputText.text.toString()
            }

            row.addView(textView)
            tableView.addView(row)

            Toast.makeText(this, "Uspešno dodat red!", Toast.LENGTH_SHORT).show()
        }
    }

    fun addStringToTable(){
        for (ime in tableContent) {
            val row = TableRow(this)
            row.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            row.gravity = Gravity.CENTER_HORIZONTAL

            val textView = TextView(this)
            textView.textSize = 30f
            textView.setTextColor(Color.DKGRAY)
            textView.setTypeface(Typeface.DEFAULT_BOLD);
            textView.setBackgroundResource(R.color.grey)
            textView.gravity = Gravity.CENTER_HORIZONTAL
            textView.setPadding(20)
            textView.apply {
                textView.text = ime
            }

            row.addView(textView)
            tableView.addView(row)
        }
    }

    fun createDatasource() {
        tableContent.add("Jovana")
        tableContent.add("Marko")
        tableContent.add("Anđela")
        tableContent.add("Tea")
        tableContent.add("Nemanja")
        tableContent.add("Jovana")
        tableContent.add("Marko")
        tableContent.add("Anđela")
        tableContent.add("Tea")
        tableContent.add("Nemanja")
        tableContent.add("Jovana")
        tableContent.add("Marko")
        tableContent.add("Anđela")
        tableContent.add("Tea")
        tableContent.add("Nemanja")
    }
}