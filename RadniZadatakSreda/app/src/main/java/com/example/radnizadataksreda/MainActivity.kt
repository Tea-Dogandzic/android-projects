package com.example.radnizadataksreda

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var otvoren: Boolean = false

        prikaziMeni.setOnClickListener{
            if(!otvoren) {
                padajuciMeni.visibility = View.VISIBLE
                prikaziMeni.text = getString(R.string.label_zatvori_meni)
                println(otvoren)
            } else {
                padajuciMeni.visibility = View.GONE
                prikaziMeni.text = getString(R.string.label_otvori_meni)
                println(otvoren)
            }
            otvoren = !otvoren
        }

        zameniPlavo.setOnClickListener {
            zameniZeleno.setBackgroundResource(R.drawable.rectangle_red)
            zameniCrveno.setBackgroundResource(R.drawable.rectangle_red)
            zameniPlavo.setBackgroundResource(R.color.blue)
        }

        zameniZeleno.setOnClickListener {
            zameniPlavo.setBackgroundResource(R.drawable.rectangle_red)
            zameniCrveno.setBackgroundResource(R.drawable.rectangle_red)
            zameniZeleno.setBackgroundResource(R.color.green)
        }

        zameniCrveno.setOnClickListener {
            zameniZeleno.setBackgroundResource(R.drawable.rectangle_red)
            zameniPlavo.setBackgroundResource(R.drawable.rectangle_red)
            zameniCrveno.setBackgroundResource(R.color.red)
        }
    }
}