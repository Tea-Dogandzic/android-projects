package it.engineering.prosledjivanjepodataka

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val left = findViewById<TextView>(R.id.leftArrow)
        val right = findViewById<TextView>(R.id.rightArrow)

        left.setOnClickListener {
            leftActive = true
            startActivity(Intent(this, LeftActivity::class.java))
        }

        right.setOnClickListener {
            rightActive = true
            startActivity(Intent(this, RightActivity::class.java))
        }
    }
}