package it.engineering.prosledjivanjepodataka

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

var rightActive: Boolean = false

class RightActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_right)

        val left = findViewById<TextView>(R.id.leftArrow)
        val right = findViewById<TextView>(R.id.rightArrow)

        if (rightActive) {
            right.setBackgroundResource(0)
        }

        left.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }

//        right.setOnClickListener {
//            startActivity(Intent(this, RightActivity::class.java))
//        }
    }
}