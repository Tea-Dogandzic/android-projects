package it.engineering.prosledjivanjepodataka

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

var leftActive: Boolean = false

class LeftActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_left)

        val left = findViewById<TextView>(R.id.leftArrow)
        val right = findViewById<TextView>(R.id.rightArrow)

        if (leftActive) {
            left.setBackgroundResource(0)
        }

//        left.setOnClickListener {
//            startActivity(Intent(this, LeftActivity::class.java))
//        }

        right.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }
}