package it.engineering.recyclerviewgridprikaz

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.grid_item.view.*

class recyclerAdapter(val preuzetiPodaci:ArrayList<Hero>) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.grid_item, parent,false)
        return CustomViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        return preuzetiPodaci.count()
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.title_text_view.text = preuzetiPodaci[position].ime
        holder.view.icon_image_view.setImageResource(preuzetiPodaci[position].slika)
    }
}

class CustomViewHolder(val view : View): RecyclerView.ViewHolder(view) {

}