package it.engineering.recyclerviewgridprikaz

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

data class Hero(val ime: String, val slika: Int)

class MainActivity : AppCompatActivity() {
    val objectArray: ArrayList<Hero> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        createDataSource()
        val kreiraniAdapter = recyclerAdapter(objectArray)
        recyclerView.adapter = kreiraniAdapter
        recyclerView.setLayoutManager(GridLayoutManager(this,3))
    }

    private fun createDataSource() {
        objectArray.add(Hero(getString(R.string.hero_name_1), R.drawable.ana))
        objectArray.add(Hero(getString(R.string.hero_name_2), R.drawable.ashe))
        objectArray.add(Hero(getString(R.string.hero_name_3), R.drawable.baptiste))
        objectArray.add(Hero(getString(R.string.hero_name_4), R.drawable.bastion))
        objectArray.add(Hero(getString(R.string.hero_name_5), R.drawable.brigitte))
        objectArray.add(Hero(getString(R.string.hero_name_6), R.drawable.dva))
        objectArray.add(Hero(getString(R.string.hero_name_7), R.drawable.doomfist))
        objectArray.add(Hero(getString(R.string.hero_name_8), R.drawable.echo))
        objectArray.add(Hero(getString(R.string.hero_name_9), R.drawable.genji))
        objectArray.add(Hero(getString(R.string.hero_name_10), R.drawable.hanzo))
        objectArray.add(Hero(getString(R.string.hero_name_11), R.drawable.junkrat))
        objectArray.add(Hero(getString(R.string.hero_name_12), R.drawable.lucio))
    }
}