package it.engineering.imdbclone

data class Movie(
    val voteAverage: Double,
    val voteCount: Int,
    val originalTitle: String,
    val title: String,
    val popularity: Double,
    val backdropPath: String,
    val overview: String,
    val releaseDate: String,
    val posterPath: String
)
