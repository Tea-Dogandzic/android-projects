package it.engineering.imdbclone

data class Actor(
    var name: String,
    var age: Int,
    var photoPath: String
)