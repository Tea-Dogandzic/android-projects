package it.engineering.imdbclone

object global {
    var moviesData : ArrayList<Movie> = arrayListOf()
    var streamingData: ArrayList<Movie> = arrayListOf()
    var inTheatersData: ArrayList<Movie> = arrayListOf()
    var comingSoonData: ArrayList<Movie> = arrayListOf()
    var watchSoonData: ArrayList<Movie> = arrayListOf()

    var featuredPictures : ArrayList<Int> = arrayListOf(
        R.drawable.featured1,
        R.drawable.featured2,
        R.drawable.featured3,
        R.drawable.featured4,
        R.drawable.featured5,
        R.drawable.featured6,
        R.drawable.featured7,
        R.drawable.featured8
    )

    var featuredTitles : ArrayList<String> = arrayListOf(
        "All the Best Shows Coming This Summer",
        "Celebrity Siblings: Famous Brothers and Sisters",
        "Photos We Love: Tribeca Stars Through the Years",
        "Shows Everyone's Still Talking About",
        "What TV Shows Are Renewed or Canceled?",
        "The Latest Red Carpet Photos and More",
        "Celebrate Pride With Watch Guides, Photos, Video, and More",
        "All the Latest Movie and TV Posters"
    )

    var originalsPictures : ArrayList<Int> = arrayListOf(
        R.drawable.original1,
        R.drawable.original2,
        R.drawable.original3,
        R.drawable.original4,
        R.drawable.original5,
        R.drawable.original6,
        R.drawable.original7,
        R.drawable.original8,
        R.drawable.original9,
        R.drawable.original10
    )

    var originalsTitles : ArrayList<String> = arrayListOf(
        "Is the 'Luca' Cast Just Like Their Characters IRL?",
        "7 Tribeca Festival Films We Can't Wait to See",
        "How \"Kevin Can F**k Himself\" Breaks the Sitcom Mold",
        "The 'F9' Family Shares 20 Years of Fast & Furious Memories",
        "The \"Panic\" Cast Talk Scary Movies and More",
        "Rita Moreno Breaks Down 6 of Her Iconic roles",
        "The Rise of Charlize Theron",
        "Shows Everyone's Still Talking About",
        "The Rise of 'F9' Star Sung Kang",
        "\"iCarly\" Stars Reveal Their Craziest Fan Encounters"
    )

    var editorsPicksPictures : ArrayList<Int> = arrayListOf(
        R.drawable.editors1,
        R.drawable.editors2,
        R.drawable.editors3,
        R.drawable.editors4,
        R.drawable.editors5,
        R.drawable.editors6,
        R.drawable.editors7,
        R.drawable.editors8,
        R.drawable.editors9,
        R.drawable.editors10,
    )

    var editorsPicksTitles : ArrayList<String> = arrayListOf(
        "The Best TV and Movies to Watch in June",
        "The Latest Red Carpet Photos and More",
        "Everything New on HBO in June",
        "Tribeca 2021: Premieres and Parties",
        "Everything New on Prime Video in June",
        "Everything New on Disney Plus in June",
        "15 Supportive Indian Cinema Dads From the Last 15 Years",
        "Everything New on Hulu in June",
        "June 2021 TV and Streaming Premiere Dates",
        "Everything New on Netflix in June"
    )

    var actorsData : ArrayList<Actor> = arrayListOf(
        Actor("Meryl Streep", 72,"https://m.media-amazon.com/images/M/MV5BMTU4Mjk5MDExOF5BMl5BanBnXkFtZTcwOTU1MTMyMw@@._V1_UY317_CR6,0,214,317_AL_.jpg"),
        Actor("Mary Lynn Rajskub", 50,"https://m.media-amazon.com/images/M/MV5BMTc5NDcwNjM1MF5BMl5BanBnXkFtZTcwNTcwNjE5NA@@._V1_UX214_CR0,0,214,317_AL_.jpg"),
        Actor("Bruce Campbell", 63,"https://m.media-amazon.com/images/M/MV5BOTY2MTg0OTEzOV5BMl5BanBnXkFtZTgwNzQ3NTcwMjE@._V1_UX214_CR0,0,214,317_AL_.jpg"),
        Actor("Amy Brenneman", 57,"https://m.media-amazon.com/images/M/MV5BODQ3MjkzNTkzNF5BMl5BanBnXkFtZTcwNTQ4MjgxNQ@@._V1_UY317_CR1,0,214,317_AL_.jpg"),
        Actor("Donald Faison", 47,"https://m.media-amazon.com/images/M/MV5BMTQ1NDQ2MzA2NV5BMl5BanBnXkFtZTcwODQ0MzUwNA@@._V1_UY317_CR9,0,214,317_AL_.jpg"),
        Actor("Douglas Smith", 36,"https://m.media-amazon.com/images/M/MV5BM2MyYzhhNjItZTlmNC00OWI1LThiOTYtZmFmYTQ5ZTBhZDU2XkEyXkFqcGdeQXVyMjc0NDg2MzI@._V1_UY317_CR17,0,214,317_AL_.jpg"),
        Actor("Michael Trucco", 51,"https://m.media-amazon.com/images/M/MV5BYzA1NDk4NDktMWYwYy00MjQzLTg0ZDAtMWJhY2UyNmI5YTEwXkEyXkFqcGdeQXVyNjA4NjQwNw@@._V1_UX214_CR0,0,214,317_AL_.jpg"),
        Actor("Lindsay Wagner", 72,"https://m.media-amazon.com/images/M/MV5BNzEyNjRjZmQtZTgzYi00YmJkLTgxZmQtYTU5ZWU3NGE4MjcwXkEyXkFqcGdeQXVyMTEwODg2MDY@._V1_UY317_CR36,0,214,317_AL_.jpg"),
        Actor("Kris Kristofferson", 85,"https://m.media-amazon.com/images/M/MV5BODM4NDY5MTYyMl5BMl5BanBnXkFtZTYwNjg1ODE2._V1_UX214_CR0,0,214,317_AL_.jpg"),
        Actor("Eve Mavrakis", 55,"https://m.media-amazon.com/images/M/MV5BMTU0MTU1MjA2N15BMl5BanBnXkFtZTcwOTMxNTg2OA@@._V1_UY317_CR27,0,214,317_AL_.jpg"),
        Actor("Graham Greene", 69,"https://m.media-amazon.com/images/M/MV5BMTcwMTM3OTQyM15BMl5BanBnXkFtZTcwNzYxNTcyMQ@@._V1_UY317_CR15,0,214,317_AL_.jpg"),
        Actor("Jeff Hephner", 46,"https://m.media-amazon.com/images/M/MV5BYjI3ZDIzNGQtM2ExNy00MWNjLWFlODYtYWY0YzgzZjBjN2QxXkEyXkFqcGdeQXVyMTUwMjcwMjY@._V1_UY317_CR4,0,214,317_AL_.jpg")
    )
}