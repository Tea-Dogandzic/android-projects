package it.engineering.imdbclone

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ActorsAdapter(var data: ArrayList<Actor>) : RecyclerView.Adapter<CustomViewHolderActorsAdapter>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolderActorsAdapter {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.actor_item, parent,false)

        return CustomViewHolderActorsAdapter(cellForRow)
    }

    override fun getItemCount(): Int {
        return data.count()
    }

    override fun onBindViewHolder(holder: CustomViewHolderActorsAdapter, position: Int) {
        val actorItem = holder.view.findViewById<LinearLayout>(R.id.actorItemLayout)
        val recyclerActorImage = holder.view.findViewById<ImageView>(R.id.recyclerActorImage)
        val recyclerActorName = holder.view.findViewById<TextView>(R.id.recyclerActorName)
        val recyclerActorAge = holder.view.findViewById<TextView>(R.id.recyclerActorAge)

        recyclerActorImage.setBackgroundResource(0)
        HomeFragment().downloadImage(recyclerActorImage).execute(data[position].photoPath)
        recyclerActorName.text = data[position].name
        recyclerActorAge.text = data[position].age.toString()

        actorItem.setOnClickListener {

        }
    }
}

class CustomViewHolderActorsAdapter(val view : View): RecyclerView.ViewHolder(view) {

}