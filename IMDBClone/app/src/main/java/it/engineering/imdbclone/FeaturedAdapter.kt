package it.engineering.imdbclone

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class FeaturedAdapter(var pictures: ArrayList<Int>, var titles: ArrayList<String>) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.featured_item, parent,false)

        return CustomViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        return pictures.count()
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val featuredItem = holder.view.findViewById<LinearLayout>(R.id.featuredLayout)
        val featuredPicture = holder.view.findViewById<ImageView>(R.id.imageFeaturedItem)
        val featuredTitle = holder.view.findViewById<TextView>(R.id.titleFeaturedItem)

        featuredPicture.setBackgroundResource(pictures[position])
        featuredTitle.text = titles[position]

        featuredItem.setOnClickListener {

        }
    }
}

class CustomViewHolder(val view : View): RecyclerView.ViewHolder(view) {

}