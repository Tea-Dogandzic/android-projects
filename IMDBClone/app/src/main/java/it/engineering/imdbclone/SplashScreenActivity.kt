package it.engineering.imdbclone

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class SplashScreenActivity : AppCompatActivity() {
    lateinit var progressBar: ProgressBar

    var movies : ArrayList<Movie> = arrayListOf()
    var firstJson = true

    val jsonUrl = "https://api.themoviedb.org/3/discover/movie?certification_country=US&certification.lte=G&sort_by=popularity.desc&api_key=e7bab9c4f505879809cef1def03c49ee"
    val jsonUrl2 = "https://api.themoviedb.org/3/discover/movie?primary_release_date.gte=2014-09-15&primary_release_date.lte=2014-10-22&api_key=e7bab9c4f505879809cef1def03c49ee"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        progressBar = findViewById(R.id.progressBar)

        downloadJSON().execute(jsonUrl)
    }

    fun parseJSON(jsonText: String) {
        val baseURL: String = "https://image.tmdb.org/t/p/w500"

        val jsonObject = JSONObject(jsonText)
        val jsonArray = jsonObject.getJSONArray("results")
        jsonArray.let {
            (0 until it.length()).forEach {
                val jsonObj = jsonArray.getJSONObject(it)

                val voteAverage = jsonObj.getDouble("vote_average")
                val voteCount = jsonObj.getInt("vote_count")
                val originalTitle = jsonObj.getString("original_title")
                val title = jsonObj.getString("title")
                val popularity = jsonObj.getDouble("popularity")
                val backdropPath = jsonObj.getString("backdrop_path")
                val overview = jsonObj.getString("overview")
                val releaseDate = jsonObj.getString("release_date")
                val posterPath = jsonObj.getString("poster_path")

                movies.add(Movie(voteAverage, voteCount, originalTitle, title, popularity, baseURL + backdropPath, overview, releaseDate, baseURL + posterPath))
            }
        }
    }

    inner class downloadJSON: AsyncTask<String, String, String>() {
        override fun doInBackground(vararg params: String?): String {
            var json: String
            val connection = URL(params[0]).openConnection() as HttpURLConnection

            try {
                connection.connect()
                json = connection.inputStream.use {
                    it.reader().use {
                            reader -> reader.readText()
                    }
                }
            } finally {
                connection.disconnect()
            }

            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            progressBar.visibility = View.GONE

            if (result != null && firstJson) {
                parseJSON(result)
                global.moviesData = movies
                movies = arrayListOf()
                firstJson = false
                downloadJSON().execute(jsonUrl2)
            } else if (result != null && !firstJson) {
                parseJSON(result)
                global.streamingData = movies
                startActivity(Intent(this@SplashScreenActivity, MainActivity::class.java))
            }
        }
    }
}