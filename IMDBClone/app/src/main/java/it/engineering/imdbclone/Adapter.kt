package it.engineering.imdbclone

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class Adapter(var data: ArrayList<Movie>, var btnText: Int, var soon: Boolean) : RecyclerView.Adapter<CustomViewHolderActorsAdapter>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolderActorsAdapter {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.movie_item, parent,false)

        return CustomViewHolderActorsAdapter(cellForRow)
    }

    override fun getItemCount(): Int {
        return data.count()
    }

    override fun onBindViewHolder(holder: CustomViewHolderActorsAdapter, position: Int) {
        val movieItem = holder.view.findViewById<LinearLayout>(R.id.movieItemLayout)
        val recyclerImage = holder.view.findViewById<ImageView>(R.id.recyclerImage)
        val recyclerRating = holder.view.findViewById<TextView>(R.id.recyclerRating)
        val recyclerTitle = holder.view.findViewById<TextView>(R.id.recyclerTitle)
        val recyclerDate = holder.view.findViewById<TextView>(R.id.recyclerDate)
        val btnPlusWatchlist = holder.view.findViewById<Button>(R.id.btnPlusWatchlist)

        recyclerImage.setBackgroundResource(0)
        HomeFragment().downloadImage(recyclerImage).execute(data[position].posterPath)
        recyclerTitle.text = data[position].title

        if (!soon) {
            recyclerRating.text = data[position].voteAverage.toString()
            recyclerDate.text = data[position].releaseDate
            btnPlusWatchlist.setText(btnText)
            btnPlusWatchlist.setOnClickListener {

            }
        } else {
            holder.view.findViewById<ImageView>(R.id.starIcon).visibility = View.GONE
            recyclerRating.visibility = View.GONE
            btnPlusWatchlist.visibility = View.GONE
            recyclerDate.text = data[position].overview.substring(0, 50) + "..."
        }

        movieItem.setOnClickListener {

        }
    }
}

class CustomViewHolderAdapter(val view : View): RecyclerView.ViewHolder(view) {

}