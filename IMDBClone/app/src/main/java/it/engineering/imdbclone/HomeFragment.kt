package it.engineering.imdbclone

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.net.URL
import kotlin.math.abs

class HomeFragment : Fragment() {
    private var currentMovieIndex: Int = 0
    private lateinit var moviesLayout: LinearLayout

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view: View = inflater.inflate(R.layout.fragment_home, container, false)

        moviesLayout = view.findViewById(R.id.moviesLayout)

        populateMoviesLayout(moviesLayout, view)
        slideImages(moviesLayout)

        val recyclerViewFeatured = view.findViewById<RecyclerView>(R.id.recyclerViewFeatured)
        val createdFeaturedAdapter = FeaturedAdapter(global.featuredPictures, global.featuredTitles)
        recyclerViewFeatured.adapter = createdFeaturedAdapter
        recyclerViewFeatured.setLayoutManager(LinearLayoutManager(view.context, LinearLayoutManager.HORIZONTAL, false))

        val recyclerViewFanFavorites = view.findViewById<RecyclerView>(R.id.recyclerViewFanFavorites)
        val createdFanFavoritesAdapter = Adapter(global.moviesData, R.string.labelPlusWatchlist, false)
        recyclerViewFanFavorites.adapter = createdFanFavoritesAdapter
        recyclerViewFanFavorites.setLayoutManager(LinearLayoutManager(view.context, LinearLayoutManager.HORIZONTAL, false))

        val recyclerViewOriginals = view.findViewById<RecyclerView>(R.id.recyclerViewOriginals)
        val createdOriginalsAdapter = FeaturedAdapter(global.originalsPictures, global.originalsTitles)
        recyclerViewOriginals.adapter = createdOriginalsAdapter
        recyclerViewOriginals.setLayoutManager(LinearLayoutManager(view.context, LinearLayoutManager.HORIZONTAL, false))

        val recyclerViewStreaming = view.findViewById<RecyclerView>(R.id.recyclerViewStreaming)
        val createdStreamingAdapter = Adapter(global.streamingData, R.string.labelWatchNow, false)
        recyclerViewStreaming.adapter = createdStreamingAdapter
        recyclerViewStreaming.setLayoutManager(LinearLayoutManager(view.context, LinearLayoutManager.HORIZONTAL, false))

        for (m in global.streamingData) {
            global.inTheatersData.add(m)
        }
        val recyclerViewInTheaters = view.findViewById<RecyclerView>(R.id.recyclerViewInTheaters)
        val createdTheatersAdapter = Adapter(shuffle(global.inTheatersData), R.string.labelShowtimes, false)
        recyclerViewInTheaters.adapter = createdTheatersAdapter
        recyclerViewInTheaters.setLayoutManager(LinearLayoutManager(view.context, LinearLayoutManager.HORIZONTAL, false))

        for (m in global.moviesData) {
            global.comingSoonData.add(m)
        }
        val recyclerViewComingSoon = view.findViewById<RecyclerView>(R.id.recyclerViewComingSoon)
        val createdComingSoonAdapter = Adapter(shuffle(global.comingSoonData), 0, true)
        recyclerViewComingSoon.adapter = createdComingSoonAdapter
        recyclerViewComingSoon.setLayoutManager(LinearLayoutManager(view.context, LinearLayoutManager.HORIZONTAL, false))

        for (m in global.streamingData) {
            global.watchSoonData.add(m)
        }
        val recyclerViewWatchSoon = view.findViewById<RecyclerView>(R.id.recyclerViewWatchSoon)
        val createdWatchSoonAdapter = Adapter(shuffle(global.watchSoonData), 0, true)
        recyclerViewWatchSoon.adapter = createdWatchSoonAdapter
        recyclerViewWatchSoon.setLayoutManager(LinearLayoutManager(view.context, LinearLayoutManager.HORIZONTAL, false))

        val recyclerViewEditorsPicks = view.findViewById<RecyclerView>(R.id.recyclerViewEditorsPicks)
        val createdEditorsPicksAdapter = FeaturedAdapter(global.editorsPicksPictures, global.editorsPicksTitles)
        recyclerViewEditorsPicks.adapter = createdEditorsPicksAdapter
        recyclerViewEditorsPicks.setLayoutManager(LinearLayoutManager(view.context, LinearLayoutManager.HORIZONTAL, false))

        val recyclerViewActors = view.findViewById<RecyclerView>(R.id.recyclerViewBornToday)
        val createdActorsAdapter = ActorsAdapter(global.actorsData)
        recyclerViewActors.adapter = createdActorsAdapter
        recyclerViewActors.setLayoutManager(LinearLayoutManager(view.context, LinearLayoutManager.HORIZONTAL, false))

        return view
    }

    fun shuffle(list: MutableList<Movie>): ArrayList<Movie> {
        list.shuffle()

        return list as ArrayList<Movie>
    }

    private fun slideImages(v: View) {
        v.setOnTouchListener(
            object : View.OnTouchListener {
                var x1: Float = 0.0f
                var x2: Float = 0.0f

                val MIN_DISTANCE = 300

                override fun onTouch(v: View?, event: MotionEvent): Boolean {
                    when(event!!.action) {
                        0 -> {
                            x1 = event.x
                        }

                        1 -> {
                            x2 = event.x

                            val valueX: Float = x2 - x1

                            if (abs(valueX) > MIN_DISTANCE){
                                if (x2 > x1) {
                                    Log.d("SWIPE", "RIGHT")
                                    currentMovieIndex--
                                    if (currentMovieIndex == -1) {
                                        currentMovieIndex = global.moviesData.count() - 1
                                    }
                                    populateMoviesLayout(moviesLayout, v!!)
                                } else {
                                    Log.d("SWIPE", "LEFT")
                                    currentMovieIndex++
                                    if (currentMovieIndex == global.moviesData.count()) {
                                        currentMovieIndex = 0
                                    }
                                    populateMoviesLayout(moviesLayout, v!!)
                                }
                            }
                        }
                    }

                    return true
                }
            }
        )
    }

    private fun populateMoviesLayout(vItem: LinearLayout, v: View) {
        var imageViewBackPic: ImageView = v.findViewById(R.id.imageViewBackPic)
        var imageViewPosterPic: ImageView = v.findViewById(R.id.imageViewPosterPic)
        var textViewTitle: TextView = v.findViewById(R.id.textViewTitle)

        imageViewBackPic.setBackgroundResource(0)
        imageViewPosterPic.setBackgroundResource(0)

        downloadImage(imageViewBackPic).execute(global.moviesData[currentMovieIndex].backdropPath)
        downloadImage(imageViewPosterPic).execute(global.moviesData[currentMovieIndex].posterPath)
        textViewTitle.text = global.moviesData[currentMovieIndex].title

        vItem.setOnClickListener {

        }
    }

    inner class downloadImage(val vImage: ImageView): AsyncTask<String, Void, Bitmap?>() {
        override fun doInBackground(vararg params: String?): Bitmap? {
            var image: Bitmap? = null

            val url = params[0]

            try {
                val downloadedImage = URL(url).openStream()
                image = BitmapFactory.decodeStream(downloadedImage)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return image
        }

        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)

            vImage.setImageBitmap(result)
        }
    }
}