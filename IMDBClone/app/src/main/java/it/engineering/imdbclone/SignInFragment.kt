package it.engineering.imdbclone

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment

class SignInFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view: View = inflater.inflate(R.layout.fragment_sign_in, container, false)

        val toolbar: Toolbar = view.findViewById(R.id.toolbar) as Toolbar
        (activity as AppCompatActivity?)!!.setSupportActionBar(toolbar)
        (activity as AppCompatActivity?)!!.supportActionBar?.setTitle("")

        val btnBack = view.findViewById<ImageView>(R.id.btnBack)
        btnBack.setOnClickListener {
            activity?.getSupportFragmentManager()?.beginTransaction()?.remove(this)?.commit();
        }

        val btnSignInImdb = view.findViewById<LinearLayout>(R.id.btnSignInImdb)
        btnSignInImdb.setOnClickListener {
            startActivity(Intent(view.context, SignInActivity::class.java))
        }

        val btnCreateAccount = view.findViewById<LinearLayout>(R.id.btnCreateAccount)
        btnCreateAccount.setOnClickListener {
            startActivity(Intent(view.context, CreateAccountActivity::class.java))
        }

        return view
    }
}