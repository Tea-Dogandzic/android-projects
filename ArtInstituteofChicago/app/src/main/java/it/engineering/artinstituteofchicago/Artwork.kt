package it.engineering.artinstituteofchicago

data class Artwork(
    val id: Int,
    val apiLink: String,
    val title: String,
    //val thumbnail: Thumbnail,
    val dateStart: Int,
    val dateEnd: Int,
    val dateDisplay: String,
    val placeOfOrigin: String,
    val dimensions: String,
    val colorfulness: Double,
    //val color: Color?,
    var isOnView: Boolean,
    val departmentTitle: String,
    val termTitles: ArrayList<String>,
    val styleTitle: String,
    val classificationTitle: String,
    var fullImageUrl: String
    ) {
    override fun toString(): String {
        return "id -> " + id + "\n" +
                "apiLink -> " + apiLink + "\n" +
                "title -> " + title + "\n" +
                "dateStart -> " + dateStart + "\n" +
                "dateEnd -> " + dateEnd + "\n" +
                "dateDisplay -> " + dateDisplay + "\n" +
                "placeOfOrigin -> " + placeOfOrigin + "\n" +
                "dimensions -> " + dimensions + "\n" +
                "colorfulness -> " + colorfulness + "\n" +
                "isOnView -> " + isOnView + "\n" +
                "departmentTitle -> " + departmentTitle + "\n" +
                "termTitles -> " + termTitles + "\n" +
                "styleTitle -> " + styleTitle + "\n" +
                "classificationTitle -> " + classificationTitle + "\n" +
                "fullImageUrl -> " + fullImageUrl
    }
}