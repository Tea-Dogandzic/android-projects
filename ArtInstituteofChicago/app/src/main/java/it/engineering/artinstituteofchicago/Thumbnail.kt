package it.engineering.artinstituteofchicago

data class Thumbnail (
    val lgip: String,
    val width: Double,
    val height: Double,
    val altText: String
    )