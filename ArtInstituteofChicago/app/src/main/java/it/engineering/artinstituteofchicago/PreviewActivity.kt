package it.engineering.artinstituteofchicago

import android.opengl.Visibility
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.core.view.isVisible
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

class PreviewActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    var spinner: Spinner? = null
    var arrayAdapter: ArrayAdapter<String>? = null
    var categories: ArrayList<String> = arrayListOf()
    var selectedCategory: String? = null
    var filteredArtworks: ArrayList<Artwork> = arrayListOf()

    var izlozeniIspis: TextView? = null
    var btnSee: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_preview)

        showRecyclerView(global.artworksWithImages)

        categories = global.categoriesList

        izlozeniIspis = findViewById(R.id.izlozeniIspis)
        btnSee = findViewById(R.id.btnSee)
        (btnSee as Button).isVisible = false

        spinner = findViewById(R.id.category_spinner)
        arrayAdapter = ArrayAdapter(applicationContext, R.layout.support_simple_spinner_dropdown_item, categories)
        spinner?.adapter = arrayAdapter
        spinner?.onItemSelectedListener = this
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        selectedCategory = parent?.getItemAtPosition(position) as String
        if (!selectedCategory.equals(categories[0])) {
            filterByCategory(selectedCategory!!)
            showRecyclerView(filteredArtworks)
            if (filteredArtworks.count() == 2) {
                filteredArtworks[1].isOnView = true
                //filteredArtworks[2].isOnView = true
            }
            izlozeni()
        } else {
            showRecyclerView(global.artworksWithImages)
            izlozeniIspis?.text = ""
            (btnSee as Button).isVisible = false
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        Toast.makeText(this, "Nothing Selected", Toast.LENGTH_LONG).show()
    }

    fun filterByCategory(catName: String) {
        filteredArtworks = arrayListOf()

        filteredArtworks = global.artworksWithImages.filter { a -> a.classificationTitle.equals(catName) } as ArrayList<Artwork>
    }

    fun showRecyclerView(array: ArrayList<Artwork>) {
        val kreiraniAdapter = Adapter(array)
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.adapter = kreiraniAdapter
        recyclerView.setLayoutManager(GridLayoutManager(this, 1))
    }

    fun izlozeni() {
        var izlozeni: ArrayList<Artwork> = arrayListOf()
        izlozeni = filteredArtworks.filter { a -> a.isOnView == true } as ArrayList<Artwork>

        if (izlozeni.count() == 0) {
            izlozeniIspis?.text = "Nema izlozenih dela trenutno!"
            (btnSee as Button).isVisible = false
        } else {
            var ispis = "Trenutno izlozena dela su: \n"

            for (i in izlozeni) {
                ispis += "♦" + i.title + "\n"
            }

            izlozeniIspis?.text = ispis
            (btnSee as Button).isVisible = true
            (btnSee as Button).setOnClickListener {
                showRecyclerView(izlozeni)
            }
        }
    }
}