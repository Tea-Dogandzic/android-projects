package it.engineering.artinstituteofchicago

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.Spinner
import android.widget.TextView
import com.squareup.picasso.Picasso

class DetailsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        val prikazSlike = findViewById<ImageView>(R.id.prikazSlike)
        val prikazAtributa = findViewById<TextView>(R.id.prikazAtributa)

        Picasso.with(this).load(global.selectedArtwork.fullImageUrl).into(prikazSlike)
        prikazAtributa.text = global.selectedArtwork.toString()
    }
}