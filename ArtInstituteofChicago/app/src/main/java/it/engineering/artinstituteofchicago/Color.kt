package it.engineering.artinstituteofchicago

data class Color (
    val h: Int,
    val l: Int,
    val s: Int,
    val percentage: Double,
    val population: Int
    )