package it.engineering.artinstituteofchicago

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.json.JSONObject
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL

class MainActivity : AppCompatActivity() {
    val artworks : ArrayList<Artwork> = arrayListOf()
    lateinit var btnAll: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val jsonUrl = "https://api.artic.edu/api/v1/artworks?page=9"
        preuzmiJSON().execute(jsonUrl)

        btnAll = findViewById(R.id.btnAll)
        btnAll.setOnClickListener {
            startActivity(Intent(this, PreviewActivity::class.java))
        }
    }

    fun parseJSON(jsonText: String) {
        try {
            val jsonObject = JSONObject(jsonText)
            val jsonNiz = jsonObject.getJSONArray("data")
            jsonNiz.let {
                (0 until it.length()).forEach {
                    val jsonObjekat = jsonNiz.getJSONObject(it)

                    val id = jsonObjekat.getInt("id")
                    val apiLink = jsonObjekat.getString("api_link")
                    val title = jsonObjekat.getString("title")
                    //val thumbnail = jsonObjekat.getJSONObject("thumbnail")
                    val dateStart = jsonObjekat.getInt("date_start")
                    val dateEnd = jsonObjekat.getInt("date_end")
                    val dateDisplay = jsonObjekat.getString("date_display")
                    val placeOfOrigin = jsonObjekat.getString("place_of_origin")
                    val dimensions = jsonObjekat.getString("dimensions")
                    val colorfulness = jsonObjekat.getDouble("colorfulness")
                    val isOnView = jsonObjekat.getBoolean("is_on_view")
                    val departmentTitle = jsonObjekat.getString("department_title")
                    val termTitles = jsonObjekat.getJSONArray("term_titles")
                    val styleTitle = jsonObjekat.getString("style_title")
                    val classificationTitle = jsonObjekat.getString("classification_title")

//                    val lgip = thumbnail.getString("lqip")
//                    val width = thumbnail.getDouble("width")
//                    val height = thumbnail.getDouble("height")
//                    val altText = thumbnail.getString("alt_text")
//                    val t: Thumbnail = Thumbnail(lgip, width, height, altText)

                    val termTitlesArray: ArrayList<String> = arrayListOf()

                    termTitles.let {
                        (0 until it.length()).forEach {
                            val bonus = termTitles.getString(it)

                            termTitlesArray.add(bonus)
                        }
                    }

                    if (!global.categoriesList.contains(classificationTitle)) global.categoriesList.add(classificationTitle)

                    artworks.add(Artwork(id, apiLink, title, dateStart, dateEnd, dateDisplay, placeOfOrigin, dimensions, colorfulness, isOnView, departmentTitle, termTitlesArray, styleTitle, classificationTitle, ""))

                    val url = "https://api.artic.edu/api/v1/artworks/" + id.toString() + "?fields=id,title,image_id"
                    preuzmiJSON2(id).execute(url)
                }

                for (a in artworks) {
                    Log.d("ART1", a.toString())
                }
            }
        } catch (e: IOException){
            Log.d("JSON Exception", e.toString())
        }
    }

    inner class preuzmiJSON: AsyncTask<String, String, String>() {
        override fun doInBackground(vararg params: String?): String {
            var json: String
            val connection = URL(params[0]).openConnection() as HttpURLConnection

            try {
                connection.connect()
                json = connection.inputStream.use {
                    it.reader().use {
                            reader -> reader.readText()
                    }
                }
            } finally {
                connection.disconnect()
            }

            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            if (result != null) {
                parseJSON(result)
            }
        }
    }

    inner class preuzmiJSON2(var artworkId: Int): AsyncTask<String, String, String>() {
        override fun doInBackground(vararg params: String?): String {
            var json: String
            val connection = URL(params[0]).openConnection() as HttpURLConnection

            try {
                connection.connect()
                json = connection.inputStream.use {
                    it.reader().use {
                            reader -> reader.readText()
                    }
                }
            } finally {
                connection.disconnect()
            }

            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            if (result != null) {
                parseJSON2(result, artworkId)
            }

            Log.d("AR3", "KRAJ")
        }
    }

    fun parseJSON2(jsonText: String, artworkId: Int) {
        try {
            val jsonObject = JSONObject(jsonText)
            val jsonObjekat = jsonObject.getJSONObject("data")
            val imageId = jsonObjekat.getString("image_id")

            val imageUrl = "https://www.artic.edu/iiif/2/" + imageId.toString() + "/full/843,/0/default.jpg"

            for (a in artworks) {
                if (a.id == artworkId) {
                    a.fullImageUrl = imageUrl
                    Log.d("AR2", a.toString())
                    global.artworksWithImages.add(a)
                }
            }
        } catch (e: IOException){
            Log.d("JSON Exception", e.toString())
        }
    }

    inner class preuzmiSliku(val okvirZaSliku: ImageView): AsyncTask<String, Void, Bitmap?>() {
        override fun doInBackground(vararg params: String?): Bitmap? {
            var image: Bitmap? = null

            val urlZaPreuzimanje = params[0]

            try {
                val slikaSaInterneta = URL(urlZaPreuzimanje).openStream()
                image = BitmapFactory.decodeStream(slikaSaInterneta)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return image
        }

        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)

            okvirZaSliku.setImageBitmap(result)
        }
    }
}