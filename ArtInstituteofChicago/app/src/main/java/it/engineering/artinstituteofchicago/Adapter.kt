package it.engineering.artinstituteofchicago

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import java.net.URL

class Adapter(val data: ArrayList<Artwork>) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.item, parent,false)

        return CustomViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        return data.count()
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val slika = holder.view.findViewById<ImageView>(R.id.prikazSlike)
        preuzmiSliku(slika).execute(data[position].fullImageUrl)

        val item = holder.view.findViewById<LinearLayout>(R.id.item)
        item.setOnClickListener {
            global.selectedArtwork = data[position]
            ContextCompat.startActivity(
                holder.view.context,
                Intent(holder.view.context, DetailsActivity::class.java),
                null
            )
        }
    }

    inner class preuzmiSliku(val okvirZaSliku: ImageView): AsyncTask<String, Void, Bitmap?>() {
        override fun doInBackground(vararg params: String?): Bitmap? {
            var image: Bitmap? = null

            val urlZaPreuzimanje = params[0]

            try {
                val slikaSaInterneta = URL(urlZaPreuzimanje).openStream()
                image = BitmapFactory.decodeStream(slikaSaInterneta)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return image
        }

        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)

            okvirZaSliku.setImageBitmap(result)
        }
    }
}

class CustomViewHolder(val view : View): RecyclerView.ViewHolder(view) {

}