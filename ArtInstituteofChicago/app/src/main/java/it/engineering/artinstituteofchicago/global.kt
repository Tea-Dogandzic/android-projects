package it.engineering.artinstituteofchicago

object global {
    val artworksWithImages : ArrayList<Artwork> = arrayListOf()
    var selectedArtwork: Artwork = Artwork(0, "", "", 0, 0,
        "", "", "", 0.0, false, "",
        arrayListOf(), "", "", "")
    var categoriesList: ArrayList<String> = arrayListOf("Filter By Category")
}