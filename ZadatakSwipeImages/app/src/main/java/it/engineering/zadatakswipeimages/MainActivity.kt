package it.engineering.zadatakswipeimages

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import kotlin.math.abs

class MainActivity : AppCompatActivity(), GestureDetector.OnGestureListener {

    var nizSlika: ArrayList<Int> = arrayListOf()
    var nizBoja: ArrayList<Int> = arrayListOf()
    var brojacBoje = 0
    var brojacSlike = 0
    var alert = false

    lateinit var gestureDetector: GestureDetector

    var x1: Float = 0.0f
    var x2: Float = 0.0f
    var y1: Float = 0.0f
    var y2: Float = 0.0f

    val MIN_DISTANCA = 300

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        gestureDetector = GestureDetector(this, this)
    }

    private fun napraviSliku() {
        val layout = findViewById<LinearLayout>(R.id.mainLayout)
        val image = ImageView(this)

        image.id = R.id.prikazSlike

        image.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 950)
        image.scaleType = ImageView.ScaleType.CENTER

        layout.addView(image)

        image.setBackgroundColor(Color.parseColor("#990000"))
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event!!.action){
            0 -> {
                x1 = event.x
                y1 = event.y
            }

            1 -> {
                x2 = event.x
                y2 = event.y

                val vrednostX: Float = x2 - x1
                val vrednostY: Float = y2 - y1

                if (abs(vrednostX) > MIN_DISTANCA){
                    if (x2 > x1) {
                        alert = false
                        //Log.d("SWIPE", "DESNO")

                        prikaziPrethodnu()
                    } else {
                        //Log.d("SWIPE", "LEVO")
                        alert = false
                        prikaziSledecu()
                    }
                } else if (abs(vrednostY) > MIN_DISTANCA){
                    if(y2 > y1) {
                        Log.d("SWIPE", "DOLE")
                        alertChangingColors()
                    } else {
                        Log.d("SWIPE", "GORE")
                        ubaciSlikeIPrikazi()
                    }
                }
            }
        }

        return gestureDetector.onTouchEvent(event)
    }

    fun ubaciSlikeIPrikazi() {
        nizSlika.add(R.drawable.wall1)
        nizSlika.add(R.drawable.wall2)
        nizSlika.add(R.drawable.wall3)
        nizSlika.add(R.drawable.wall4)

        val image = findViewById<ImageView>(R.id.prikazSlike)
        image.setBackgroundColor(0)
        image.setBackgroundResource(nizSlika[0])
    }

    fun alertChangingColors() {
        Toast.makeText(this, "Mozete promeniti sliku prevlacenjem gore!", Toast.LENGTH_LONG).show()
        alert = true

        nizBoja.add(Color.BLUE)
        nizBoja.add(Color.MAGENTA)
        nizBoja.add(Color.YELLOW)

        //brojacBoje = 0
        prikaziBoju()
    }

    fun prikaziBoju() {
        val slika = findViewById<ImageView>(R.id.prikazSlike)

        if (alert) {
            Handler(Looper.getMainLooper()).postDelayed({
                slika.setBackgroundResource(0)
                slika.setBackgroundColor(menjajBoju())
                prikaziBoju()
            }, 500)
            Log.d("PrikazBoje", "Boja -> " + brojacBoje)
        } else {
            slika.setBackgroundColor(Color.TRANSPARENT)
            slika.setBackgroundResource(nizSlika[brojacSlike])
        }
    }

    fun menjajBoju() : Int {
        if (brojacBoje == 3){
            brojacBoje = 0
        }

        val boja = nizBoja[brojacBoje]
        brojacBoje += 1

        return boja
    }

    fun prikaziSledecu() {
        brojacSlike++

        if (brojacSlike == 4) {
            brojacSlike = 0
        }

        val image = findViewById<ImageView>(R.id.prikazSlike)
        image.setBackgroundResource(nizSlika[brojacSlike])
        Log.d("Promenjana slika", "Slika -> " + brojacSlike)
    }

    fun prikaziPrethodnu() {
        brojacSlike--

        if (brojacSlike == -1) {
            brojacSlike = 3
        }

        val image = findViewById<ImageView>(R.id.prikazSlike)
        image.setBackgroundResource(nizSlika[brojacSlike])
        Log.d("Promenjana slika", "Slika -> " + brojacSlike)
    }

    override fun onDown(e: MotionEvent?): Boolean {
        return false
    }

    override fun onShowPress(e: MotionEvent?) {

    }

    override fun onSingleTapUp(e: MotionEvent?): Boolean {
        return false
    }

    override fun onScroll(
        e1: MotionEvent?,
        e2: MotionEvent?,
        distanceX: Float,
        distanceY: Float
    ): Boolean {
        return false
    }

    override fun onLongPress(e: MotionEvent?) {
        Log.d("SWIPE", "DUGI KLIK")
        napraviSliku()
    }

    override fun onFling(
        e1: MotionEvent?,
        e2: MotionEvent?,
        velocityX: Float,
        velocityY: Float
    ): Boolean {
        return false
    }
}