package it.engineering.katalogautomobila

import android.content.Context
import android.util.Log
import java.io.BufferedReader
import java.io.InputStreamReader
import java.text.DecimalFormat
import java.text.NumberFormat

class AutomobiliData {
    fun parseCSV(context: Context) {
        val objectArray : ArrayList<List<String>> = arrayListOf()
        var naziviAtributa: ArrayList<String> = arrayListOf()

        var linija : String?

        val otvoriCSV = InputStreamReader(context.assets.open("modelExcelNew.csv"))
        val procitajLiniju = BufferedReader(otvoriCSV)

        naziviAtributa = procitajLiniju.readLine().split(";") as ArrayList<String>

        while (procitajLiniju.readLine().also {
                linija = it
            } != null) {

            val red : List<String> = linija!!.split(";")

            if (red[0].isEmpty()) {
                Log.d("CSV", "Red je prazan!")
            } else {
                objectArray.add(red)
            }
        }

        global.data = objectArray
        global.dataAttrs = naziviAtributa
    }

    fun convertPrice(stringPrice: String): String {
        val formatter: NumberFormat = DecimalFormat("#,###")

        stringPrice.replace(" ", "")

        if (stringPrice.contains(",")) {
            var number = stringPrice.split(",")
            return formatter.format(number[0].toDouble()) + "." + number[1]
        }
        return formatter.format(stringPrice.toDouble()) + ".00"
    }

    fun searchData(searchedText: String): ArrayList<List<String>> {
        var searchArray : ArrayList<List<String>> = arrayListOf()

        for (m in global.data) {
            val searchTerm = m[0] + m[1]
            if (searchTerm.replace(" ", "").contains(searchedText.replace(" ", ""), true) && !searchArray.contains(m)) {
                searchArray.add(m)
            }
        }

        return searchArray
    }

    fun showModels(marka: String): ArrayList<List<String>> {
        return global.data.filter { it -> it[0] == marka } as ArrayList<List<String>>
    }
}