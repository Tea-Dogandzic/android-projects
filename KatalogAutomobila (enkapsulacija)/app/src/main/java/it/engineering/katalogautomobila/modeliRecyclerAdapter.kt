package it.engineering.katalogautomobila

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import java.util.Random

class modelRecyclerAdapter(private val modeliArray: ArrayList<List<String>>) : RecyclerView.Adapter<CustomViewHolderModel>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolderModel {
        val layoutInflater = LayoutInflater.from(parent.context)
        val recycleViewRow = layoutInflater.inflate(R.layout.model_item, parent, false)
        return CustomViewHolderModel(recycleViewRow)
    }

    override fun getItemCount(): Int {
        return modeliArray.count()
    }

    override fun onBindViewHolder(holder: CustomViewHolderModel, position: Int) {
        val modelLogo = holder.view.findViewById<ImageView>(R.id.icon_image_view)
        val modelTitle = holder.view.findViewById<TextView>(R.id.title_text_view)
        val modelPrice = holder.view.findViewById<TextView>(R.id.price_text_view)
        val modelView = holder.view.findViewById<LinearLayout>(R.id.modelPrikaz)

        var indexSlike: Int = rand(0, global.modelImages.size - 1)
        modelLogo.setImageResource(global.modelImages[indexSlike])
        modelTitle.text = modeliArray[position][1] // model
        modelPrice.text = AutomobiliData().convertPrice(modeliArray[position][4]) + " $" // cena

        modelView.setOnClickListener {
            val intent = Intent(holder.view.context, DetailsActivity::class.java).apply {
                putExtra("MODEL", modeliArray[position] as ArrayList<String>)
                putExtra("INDEXSLIKE", indexSlike)
            }
            startActivity(holder.view.context, intent, null)
        }
    }
}

class CustomViewHolderModel(val view: View) : RecyclerView.ViewHolder(view) {

}

fun rand(from: Int, to: Int): Int {
    return Random().nextInt(to - from + 1) + from
}