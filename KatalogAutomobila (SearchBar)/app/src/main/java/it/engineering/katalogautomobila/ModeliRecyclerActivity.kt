package it.engineering.katalogautomobila

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

class ModeliRecyclerActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_modeli_recycler)

        var slikaMarke = findViewById<ImageView>(R.id.prikazSlike)
        slikaMarke.setBackgroundResource(global.markaZaPrikaz.image)

        val adapter = modelRecyclerAdapter(global.automobiliData.showModels(global.markaZaPrikaz.title))
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerViewModeli)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false)
    }
}