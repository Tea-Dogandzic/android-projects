package it.engineering.katalogautomobila.models

data class Marka (
    var title: String,
    var image: Int
)