package it.engineering.katalogautomobila

import it.engineering.katalogautomobila.models.Marka
import java.text.DecimalFormat
import java.text.NumberFormat

object global {
    val user: String = "tea"
    val pass: String = "123"

    var inputUser: String = ""
    var inputPass: String = ""

    var markaZaPrikaz: Marka = Marka("", 0)

    var data : ArrayList<List<String>> = arrayListOf()
    var dataAttrs: ArrayList<String> = arrayListOf()

    val modelImages: ArrayList<Int> = arrayListOf(R.drawable.model1, R.drawable.model2, R.drawable.model3)

    val automobiliData = AutomobiliData()
}