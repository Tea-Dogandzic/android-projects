package it.engineering.katalogautomobila

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import it.engineering.katalogautomobila.models.Marka

class markeRecyclerAdapter(private val markeArray: ArrayList<Marka>) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val recycleViewRow = layoutInflater.inflate(R.layout.marka_item, parent, false)
        return CustomViewHolder(recycleViewRow)
    }

    override fun getItemCount(): Int {
        return markeArray.count()
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val markaTitle = holder.view.findViewById<TextView>(R.id.title_text_view)
        val markaLogo = holder.view.findViewById<ImageView>(R.id.icon_image_view)
        val markaView = holder.view.findViewById<LinearLayout>(R.id.markaPrikaz)

        markaTitle.text = markeArray[position].title
        markaLogo.setImageResource(markeArray[position].image)

        markaView.setOnClickListener {
            global.markaZaPrikaz = markeArray[position]
            startActivity(holder.view.context, Intent(holder.view.context, ModeliRecyclerActivity::class.java), null)
        }
    }
}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}