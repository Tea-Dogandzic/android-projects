package it.engineering.katalogautomobila

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //checkLogin()

        AutomobiliData().parseCSV(this)

        val btnSeeAllModels = findViewById<Button>(R.id.btnAllModels)

        btnSeeAllModels.setOnClickListener {
            startActivity(Intent(this, MarkeRecyclerActivity::class.java))
        }
    }

    fun checkLogin() {
        if (!global.user.equals(global.inputUser) || !global.pass.equals(global.inputPass)) {
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }
}