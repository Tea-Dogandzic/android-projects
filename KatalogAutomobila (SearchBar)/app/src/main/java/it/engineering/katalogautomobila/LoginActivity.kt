package it.engineering.katalogautomobila

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val inputUser = findViewById<EditText>(R.id.inputUser)
        val inputPass = findViewById<EditText>(R.id.inputPass)
        val btnLogin = findViewById<Button>(R.id.btnLogin)

        btnLogin.setOnClickListener {
            if (!inputUser.equals("") && !inputPass.equals("")) {
                global.inputUser = inputUser.text.toString()
                global.inputPass = inputPass.text.toString()

                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }
        }
    }
}