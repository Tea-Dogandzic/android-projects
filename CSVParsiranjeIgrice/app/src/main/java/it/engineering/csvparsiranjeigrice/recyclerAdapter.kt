package it.engineering.csvparsiranjeigrice

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.grid_item.view.*

class recyclerAdapter(val preuzetiPodaci:ArrayList<Igra>) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.grid_item, parent,false)
        return CustomViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        return preuzetiPodaci.count()
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.title_text_view.text = preuzetiPodaci[position].ime

        Picasso.with(holder.view.context).load(preuzetiPodaci[position].slika).into(holder.view.icon_image_view)
        holder.view.genre_text_view.text = preuzetiPodaci[position].zanr
        holder.view.rating_text_view.text = "♥ " + preuzetiPodaci[position].ocena.toString()
        holder.view.price_text_view.text = preuzetiPodaci[position].cena.toString() + " $"

        holder.view.setOnClickListener {
            Toast.makeText(holder.view.context, position.toString(), Toast.LENGTH_SHORT).show();
            DataGlobal.igra = preuzetiPodaci[position]
            startActivity(holder.view.context, Intent(holder.view.context, DetailsActivity::class.java), null)
        }
    }
}

class CustomViewHolder(val view : View): RecyclerView.ViewHolder(view) {

}