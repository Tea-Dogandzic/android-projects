package it.engineering.csvparsiranjeigrice

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

class DetailsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        var igra: Igra = DataGlobal.igra

        var slika = findViewById<ImageView>(R.id.prikazSlike)
        var ime = findViewById<TextView>(R.id.prikazImena)
        var zanr = findViewById<TextView>(R.id.prikazZanra)
        var ocena = findViewById<TextView>(R.id.prikazOcene)
        var cena = findViewById<TextView>(R.id.prikazCene)

        Picasso.with(this).load(igra.slika).into(slika)
        ime.text = igra.ime
        zanr.text = igra.zanr
        ocena.text = "♥ " + igra.ocena.toString()
        cena.text = igra.cena.toString() + " $"
    }
}