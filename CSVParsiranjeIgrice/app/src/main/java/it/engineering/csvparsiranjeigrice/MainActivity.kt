package it.engineering.csvparsiranjeigrice

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import java.io.BufferedReader
import java.io.InputStreamReader

data class Igra(val slika: String, val ime: String, val ocena: Int, val zanr: String, val cena: Double)

class MainActivity : AppCompatActivity() {
    var igre: ArrayList<Igra> = arrayListOf()
    var igre2: ArrayList<Igra> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        parseCSV()

//        for (i in igre) {
//            if (i.ime == "The Sims 4") {
//                igre2.add(i)
//            }
//        }

        val kreiraniAdapter = recyclerAdapter(igre.filter { it -> it.ime == "The Sims 4" } as ArrayList<Igra>)
        recyclerView.adapter = kreiraniAdapter
        recyclerView.setLayoutManager(GridLayoutManager(this,2))
    }

    private fun parseCSV() {
        var linija : String?

        val otvoriCSV = InputStreamReader(assets.open("igrice.csv"))
        val procitajLiniju = BufferedReader(otvoriCSV)

        while (procitajLiniju.readLine().also {
                linija = it
            } != null) {

            val red : List<String> = linija!!.split(",")

            if (red[0].isEmpty()) {
                Log.d("CSV", "Red je prazan!")
            } else {
                igre.add(Igra(red[0], red[1], red[2].toInt(), red[3], red[4].toDouble()))
            }
        }
    }
}