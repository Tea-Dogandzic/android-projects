package it.engineering.serverlocation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var otvoren: Boolean = false

        prikaziListu.setOnClickListener{
            if(!otvoren) {
                padajucaLista.visibility = View.VISIBLE
                prikaziListu.text = getString(R.string.label_scroll_up)
                strelica.setBackgroundResource(R.drawable.ic_baseline_keyboard_arrow_up_24)
            } else {
                padajucaLista.visibility = View.GONE
                prikaziListu.text = getString(R.string.label_scroll_down)
                strelica.setBackgroundResource(R.drawable.ic_baseline_keyboard_arrow_down_24)
            }
            otvoren = !otvoren
        }
    }
}