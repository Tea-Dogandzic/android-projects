package it.engineering.prosledjivanjepodataka

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView


var niz: ArrayList<Vest> = arrayListOf()

class RecycleActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycle)

        niz = intent.getSerializableExtra("VESTI") as ArrayList<Vest>

        var recView = findViewById<RecyclerView>(R.id.recyclerView)

        val kreiraniAdapter = recyclerAdapter(niz)
        recView.adapter = kreiraniAdapter
        recView.setLayoutManager(GridLayoutManager(this,1))
    }
}