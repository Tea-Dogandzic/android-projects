package it.engineering.prosledjivanjepodataka

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import java.io.Serializable

data class Vest(val naslov: String, val text: String, val slika: Int)  : Serializable
val objectArray: ArrayList<Vest> = arrayListOf()

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        createDataSource()

        var btnIntent = findViewById<Button>(R.id.btnIntent)

        btnIntent.setOnClickListener {
            val intent = Intent(this, RecycleActivity::class.java).apply {
                putExtra("VESTI", objectArray)
            }
            startActivity(intent)
        }
    }

    private fun createDataSource() {
        objectArray.add(Vest(getString(R.string.naslov1), getString(R.string.tekst1), R.drawable.vest1))
        objectArray.add(Vest(getString(R.string.naslov2), getString(R.string.tekst2), R.drawable.vest2))
        objectArray.add(Vest(getString(R.string.naslov3), getString(R.string.tekst3), R.drawable.vest3))
        objectArray.add(Vest(getString(R.string.naslov4), getString(R.string.tekst4), R.drawable.vest4))
        objectArray.add(Vest(getString(R.string.naslov5), getString(R.string.tekst5), R.drawable.vest5))
    }

}