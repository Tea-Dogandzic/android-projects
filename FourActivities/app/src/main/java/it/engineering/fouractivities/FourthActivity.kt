package it.engineering.fouractivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class FourthActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fourth)

        val dugme1 = findViewById<Button>(R.id.btnOne)
        val dugme2 = findViewById<Button>(R.id.btnTwo)
        val dugme3 = findViewById<Button>(R.id.btnThree)

        dugme1.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
            //finish()
        }

        dugme2.setOnClickListener {
            startActivity(Intent(this, SecondActivity::class.java))
            //finish()
        }

        dugme3.setOnClickListener {
            startActivity(Intent(this, ThirdActivity::class.java))
            //finish()
        }
    }
}