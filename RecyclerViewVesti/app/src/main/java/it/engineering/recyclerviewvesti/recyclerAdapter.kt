package it.engineering.recyclerviewvesti

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_item.view.*

class recyclerAdapter(val preuzetiPodaci: ArrayList<Vest>) : RecyclerView.Adapter<CustomViewHolder>() {

    var rawIndex: Int? = null;

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.list_item, parent,false)
        return CustomViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        return preuzetiPodaci.count()
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.prikazNaslova.text = preuzetiPodaci[position].naslov
        holder.view.prikazTeksta.text = preuzetiPodaci[position].text
        holder.view.prikazSlike.setImageResource(preuzetiPodaci[position].slika)
        holder.view.setOnClickListener {
            rawIndex = position
            notifyDataSetChanged()
            Toast.makeText(holder.view.context, preuzetiPodaci[position].naslov, Toast.LENGTH_SHORT).show()
        }
        if (rawIndex == position) {
            holder.view.listItem.setCardBackgroundColor(Color.parseColor("#c1c1c1"))
        }
        else {
            holder.view.listItem.setCardBackgroundColor(Color.parseColor("#ffffff"))
        }
    }
}

class CustomViewHolder(val view : View): RecyclerView.ViewHolder(view) {

}