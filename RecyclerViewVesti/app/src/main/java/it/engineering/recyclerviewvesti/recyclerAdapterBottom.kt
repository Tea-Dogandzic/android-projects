package it.engineering.recyclerviewvesti

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_item_bottom.view.*

class recyclerAdapterBottom(val preuzetiPodaciBottom: ArrayList<BottomItem>) : RecyclerView.Adapter<CustomViewHolderBottom>() {

    var rawIndex: Int? = null;

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolderBottom {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.list_item_bottom, parent,false)
        return CustomViewHolderBottom(cellForRow)
    }

    override fun getItemCount(): Int {
        return preuzetiPodaciBottom.count()
    }

    override fun onBindViewHolder(holder: CustomViewHolderBottom, position: Int) {
        holder.view.prikazNaziva.text = preuzetiPodaciBottom[position].naslov
        holder.view.prikazIkonice.setImageResource(preuzetiPodaciBottom[position].slika)

        holder.view.setOnClickListener {
            rawIndex = position
            notifyDataSetChanged()
            Toast.makeText(holder.view.context, preuzetiPodaciBottom[position].naslov, Toast.LENGTH_SHORT).show()
        }
        if (rawIndex == position) {
            holder.view.listItemBottom.setCardBackgroundColor(Color.parseColor("#c1c1c1"))
        }
        else {
            holder.view.listItemBottom.setCardBackgroundColor(Color.parseColor("#ffffff"))
        }
    }
}

class CustomViewHolderBottom(val view : View): RecyclerView.ViewHolder(view) {

}