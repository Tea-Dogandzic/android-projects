package it.engineering.recyclerviewvesti

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

data class Vest(val naslov: String, val text: String, val slika: Int)
data class Title(val naslov: String)
data class BottomItem(val naslov: String, val slika: Int)

class MainActivity : AppCompatActivity() {

    val objectArray: ArrayList<Vest> = arrayListOf()
    val naslovi: ArrayList<Any> = arrayListOf()
    val bottomIcons: ArrayList<Any> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        createDataSource()
        createNaslovi()
        createBottom()

        val kreiraniAdapter = recyclerAdapter(objectArray)
        recyclerView.adapter = kreiraniAdapter
        recyclerView.setLayoutManager(GridLayoutManager(this,1))

        val kreiraniAdapter2 = recyclerAdapterTitles(naslovi, R.layout.list_item_title)
        recyclerViewNaslovi.adapter = kreiraniAdapter2
        recyclerViewNaslovi.setLayoutManager(LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false))

        val kreiraniAdapter3 = recyclerAdapterTitles(bottomIcons, R.layout.list_item_bottom)
        recyclerViewBottom.adapter = kreiraniAdapter3
        recyclerViewBottom.setLayoutManager(LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false))
    }

    private fun createDataSource() {
        objectArray.add(Vest(getString(R.string.naslov1), getString(R.string.tekst1), R.drawable.vest1))
        objectArray.add(Vest(getString(R.string.naslov2), getString(R.string.tekst2), R.drawable.vest2))
        objectArray.add(Vest(getString(R.string.naslov3), getString(R.string.tekst3), R.drawable.vest3))
        objectArray.add(Vest(getString(R.string.naslov4), getString(R.string.tekst4), R.drawable.vest4))
        objectArray.add(Vest(getString(R.string.naslov5), getString(R.string.tekst5), R.drawable.vest5))
    }

    private fun createNaslovi() {
        naslovi.add(Title(getString(R.string.label_naslov1)))
        naslovi.add(Title(getString(R.string.label_naslov2)))
        naslovi.add(Title(getString(R.string.label_naslov3)))
        naslovi.add(Title(getString(R.string.label_naslov4)))
        naslovi.add(Title(getString(R.string.label_naslov5)))
        naslovi.add(Title(getString(R.string.label_naslov6)))
        naslovi.add(Title(getString(R.string.label_naslov7)))
    }

    private fun createBottom() {
        bottomIcons.add(BottomItem("Home", R.drawable.ic_baseline_home_24))
        bottomIcons.add(BottomItem("Trending", R.drawable.ic_baseline_local_fire_department_24))
        bottomIcons.add(BottomItem("Subscriptions", R.drawable.ic_baseline_subscriptions_24))
        bottomIcons.add(BottomItem("Library", R.drawable.ic_baseline_folder_24))
        bottomIcons.add(BottomItem("Home", R.drawable.ic_baseline_home_24))
        bottomIcons.add(BottomItem("Trending", R.drawable.ic_baseline_local_fire_department_24))
        bottomIcons.add(BottomItem("Subscriptions", R.drawable.ic_baseline_subscriptions_24))
        bottomIcons.add(BottomItem("Library", R.drawable.ic_baseline_folder_24))
    }
}