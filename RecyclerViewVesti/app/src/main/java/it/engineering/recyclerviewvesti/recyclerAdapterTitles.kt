package it.engineering.recyclerviewvesti

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_item_bottom.view.*
import kotlinx.android.synthetic.main.list_item_title.view.*

    class recyclerAdapterTitles(val preuzetiPodaciNaslovi: ArrayList<Any>, val layout: Int) : RecyclerView.Adapter<CustomViewHolderTitles>() {

    var rawIndex: Int? = null;

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolderTitles {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(layout, parent,false)
        return CustomViewHolderTitles(cellForRow)
    }

    override fun getItemCount(): Int {
        return preuzetiPodaciNaslovi.count()
    }

    override fun onBindViewHolder(holder: CustomViewHolderTitles, position: Int) {

        if (layout == R.layout.list_item_title) {
            holder.view.textTitle.text = (preuzetiPodaciNaslovi[position] as Title).naslov

            holder.view.setOnClickListener {
                rawIndex = position
                notifyDataSetChanged()
                Toast.makeText(holder.view.context, (preuzetiPodaciNaslovi[position] as Title).naslov, Toast.LENGTH_SHORT).show()
            }
            if (rawIndex == position) {
                holder.view.listItemTitle.setCardBackgroundColor(Color.parseColor("#c1c1c1"))
            }
            else {
                holder.view.listItemTitle.setCardBackgroundColor(Color.parseColor("#ffffff"))
            }
        } else {
            holder.view.prikazNaziva.text = (preuzetiPodaciNaslovi[position] as BottomItem).naslov
            holder.view.prikazIkonice.setImageResource((preuzetiPodaciNaslovi[position] as BottomItem).slika)

            holder.view.setOnClickListener {
                rawIndex = position
                notifyDataSetChanged()
                Toast.makeText(holder.view.context, (preuzetiPodaciNaslovi[position] as BottomItem).naslov, Toast.LENGTH_SHORT).show()
            }
            if (rawIndex == position) {
                holder.view.listItemBottom.setCardBackgroundColor(Color.parseColor("#c1c1c1"))
            }
            else {
                holder.view.listItemBottom.setCardBackgroundColor(Color.parseColor("#ffffff"))
            }
        }
    }
}

class CustomViewHolderTitles(val view : View): RecyclerView.ViewHolder(view) {

}