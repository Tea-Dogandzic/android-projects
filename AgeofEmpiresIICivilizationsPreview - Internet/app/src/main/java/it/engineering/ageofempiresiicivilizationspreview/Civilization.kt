package it.engineering.ageofempiresiicivilizationspreview

data class Civilization(
    val name: String,
    val expansion: String,
    val armyType: String,
    val teamBonus: String,
    val civilizationBonus: ArrayList<String>
)
