package it.engineering.ageofempiresiicivilizationspreview

import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.json.JSONObject
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL

class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    val civilizations : ArrayList<Civilization> = arrayListOf()
    var filteredCivilizations : ArrayList<Civilization> = arrayListOf()

    var spinner: Spinner? = null
    var arrayAdapter: ArrayAdapter<String>? = null
    var itemList = arrayOf(
        "Filter By Expansion",
        "African Kingdoms",
        "Age of Kings",
        "Forgotten Empires",
        "Rise of Rajas",
        "The Conquerors"
    )
    var selectedExpansion: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val jsonUrl = "https://age-of-empires-2-api.herokuapp.com/api/v1/civilizations"
        preuzmiJSON().execute(jsonUrl)

        spinner = findViewById(R.id.expansions_spinner)
        arrayAdapter = ArrayAdapter(applicationContext, R.layout.support_simple_spinner_dropdown_item, itemList)
        spinner?.adapter = arrayAdapter
        spinner?.onItemSelectedListener = this
    }

    fun parseJSON(jsonText: String) {
        try {
            val jsonObject = JSONObject(jsonText)
            val jsonNiz = jsonObject.getJSONArray("civilizations")
            jsonNiz.let {
                (0 until it.length()).forEach {
                    val jsonObjekat = jsonNiz.getJSONObject(it)

                    val name = jsonObjekat.getString("name")
                    val expansion = jsonObjekat.getString("expansion")
                    val armyType = jsonObjekat.getString("army_type")
                    val teamBonus = jsonObjekat.getString("team_bonus")
                    val civilizationBonusJSONArray = jsonObjekat.getJSONArray("civilization_bonus")

                    val civilizationBonus: ArrayList<String> = arrayListOf()

                    civilizationBonusJSONArray.let {
                        (0 until it.length()).forEach {
                            val bonus = civilizationBonusJSONArray.getString(it)

                            civilizationBonus.add(bonus)
                        }
                    }

                    civilizations.add(Civilization(name, expansion, armyType, teamBonus, civilizationBonus))
                }

                val kreiraniAdapter = RecyclerAdapter(civilizations)
                val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
                recyclerView.adapter = kreiraniAdapter
                recyclerView.setLayoutManager(GridLayoutManager(this, 1))
            }
        } catch (e: IOException){
            Log.d("JSON Exception", e.toString())
        }
    }

    inner class preuzmiJSON: AsyncTask<String, String, String>() {
        override fun doInBackground(vararg params: String?): String {
            var json: String
            val connection = URL(params[0]).openConnection() as HttpURLConnection

            try {
                connection.connect()
                json = connection.inputStream.use {
                    it.reader().use {
                            reader -> reader.readText()
                    }
                }
            } finally {
                connection.disconnect()
            }

            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            if (result != null) {
                parseJSON(result)
            }
            println(result)
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        selectedExpansion = parent?.getItemAtPosition(position) as String
        if (!selectedExpansion.equals(itemList[0])) {
            filterByExpansion(selectedExpansion!!)
            showRecyclerView()
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        Toast.makeText(this, "Nothing Selected", Toast.LENGTH_LONG).show()
    }

    fun filterByExpansion(expName: String) {
        filteredCivilizations = arrayListOf()

        filteredCivilizations = civilizations.filter { c -> c.expansion.equals(expName) } as ArrayList<Civilization>
    }

    fun showRecyclerView() {
        val kreiraniAdapter = RecyclerAdapter(filteredCivilizations)
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.adapter = kreiraniAdapter
        recyclerView.setLayoutManager(GridLayoutManager(this, 1))
    }
}