package it.engineering.ageofempiresiicivilizationspreview

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView


class RecyclerAdapter(val data: ArrayList<Civilization>) : RecyclerView.Adapter<CustomViewHolder>() {

    var rawIndex: Int? = null;

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.item, parent,false)

        return CustomViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        return data.count()
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val item = holder.view.findViewById<CardView>(R.id.listItem)

        val slika = holder.view.findViewById<ImageView>(R.id.prikazSlike)
        val textName = holder.view.findViewById<TextView>(R.id.textName)
        val textExpansion = holder.view.findViewById<TextView>(R.id.textExpansion)
        val textArmyType = holder.view.findViewById<TextView>(R.id.textArmyType)
        val textTeamBonus = holder.view.findViewById<TextView>(R.id.textTeamBonus)
        val textCivilizationBonus = holder.view.findViewById<TextView>(R.id.textCivilizationBonus)

        val nazivSlike = data[position].expansion.replace(" ", "").lowercase()

        when (nazivSlike) {
            "theconquerors" -> slika.setBackgroundResource(R.drawable.theconquerors)
            "ageofkings" -> slika.setBackgroundResource(R.drawable.ageofkings)
            "africankingdoms" -> slika.setBackgroundResource(R.drawable.africankingdoms)
            "riseofrajas" -> slika.setBackgroundResource(R.drawable.riseofrajas)
            "forgottenempires" -> slika.setBackgroundResource(R.drawable.forgottenempires)
        }

        textName.text = "Name: " + data[position].name
        textExpansion.text = "Expansion: " + data[position].expansion
        textArmyType.text = "Army Type: " + data[position].armyType
        textTeamBonus.text = "Team Bonus: " + data[position].teamBonus

        var textCivilizationBonusArrayIspis = ""
        for (c in data[position].civilizationBonus) {
            textCivilizationBonusArrayIspis += "\n - " +  c
        }
        textCivilizationBonus.text = "Civilization Bonus: " + textCivilizationBonusArrayIspis

        holder.view.setOnClickListener {
            rawIndex = position
            notifyDataSetChanged()
        }

        if (rawIndex == position) {
            item.setCardBackgroundColor(Color.parseColor("#c1c1c1"))
        }
        else {
            item.setCardBackgroundColor(Color.parseColor("#ffffff"))
        }
    }
}

class CustomViewHolder(val view : View): RecyclerView.ViewHolder(view) {

}

fun getDrawableByName(name: String, context: Context): Drawable? {
    val drawableResource: Int = context.getResources().getIdentifier(name, "drawable", context.getPackageName())
    if (drawableResource == 0) {
        throw RuntimeException("Can't find drawable with name: $name")
    }

    return context.getResources().getDrawable(drawableResource)
}